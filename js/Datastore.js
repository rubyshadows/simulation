// holds all scripts in memory 

class Datastore {

	constructor(){
		window.datastore = this;
		this.username = '';
		this.password = '';
		this.loaded = false;
		this.loadDefaults();
		this.requestCount = 0;
		this.scriptname = '';
		this.scripttext = '';
	}

	//do not require credentials
	create(callback, data){this.postCall("create", callback, data);} 
	load(callback, data){this.getCall("load", callback, data);}
	users(callback, data){this.getCall("users", callback, data);} ///
	test(callback, data){this.postCall("test", callback, data);} //

	//require credentials
	touch(callback, data){this.postCall("touch", callback, data);}
	update(callback, data){this.postCall("update", callback, data);}
	full_restore(callback, data){this.postCall("full_restore", callback, data);} //
	heal(callback, data){this.postCall("heal", callback, data);} //

	check(callback, data){this.postCall("check", callback, data);}//
	start(callback, data){this.postCall("start", callback, data);}
	status(callback, data){this.postCall("status", callback, data);}
	delete(callback, data){this.postCall("delete", callback, data);}

	edit(callback, data){this.postCall("edit", callback, data);}
	place(callback, data){this.postCall("place", callback, data);}
	drop(callback, data) {this.postCall("drop", callback, data);} //
	collect(callback, data){this.postCall("collect", callback, data);} //
	run(callback, data){this.postCall("run", callback, data);} //
	
	cacheBust() {
		this.requestCount++;
		return "?" + this.requestCount;
	}

	loadCredentials(username, password) {
		this.username = username;
		this.password = password;
		this.loaded = true;
	}

	loadDefaults() {
		this['userData'] = {
			user: {
				id: 'collector',
				username: 'Collector',
	        	container_version: 0,
	        	container_name: '',
	        	unusual_death: false,

	        	location: window.mapLocation,
	        	character: 'collector',
	        	form: 'collector',
	        	material: 0,

	        	total_collected: 0,
	        	total_dropped: 0,
	        	row: 3,
	        	col: 3
			}
		}
	}

	storeInput(method){
		let datastore = this;
		function store(){
			datastore.scriptname = document.getElementById("actionScriptname").value;
			datastore.scripttext = document.getElementById("actionScripttext").value;
			//console.log('store: scriptname: '+datastore.scriptname)
			//console.log('store: scripttext:'+datastore.scripttext)
		}
		let methods = ['edit', 'place', 'drop', 'collect', 'run', 'test']
		for (let m of methods) if (method === m) store();
	}

	reloadCredentials(username) {
		this.username = username;
	}

	credentials(){
		return {"username": this.username, "password": this.password};
	}

	appendCredentials(obj){
		obj.username = this.username;
		obj.password = this.password;
		obj.location = window.mapLocation;
		obj.row = window.user.row;
		obj.col = window.user.col;
	}

	getCall(endpoint, callback, data){
		let query = "";
		if (data !== undefined)
			query = "/"+data;
		let service = window.dhdomain+endpoint+query+this.cacheBust();
		let xhr = new XMLHttpRequest();
		let datastore = this;
		xhr.open("GET", service, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4 && xhr.status === 200) {
	        	let json = JSON.parse(xhr.responseText);
	        	datastore[endpoint+"Data"] = json;
	        	callback(json);
	    	}
		}
		xhr.send();
	}

	postCall(endpoint, callback, data){
		this.storeInput(endpoint);
		let service = window.dhdomain+endpoint+this.cacheBust();
		let xhr = new XMLHttpRequest();
		let datastore = this;
		if (data === undefined)
			data = {}
		this.appendCredentials(data);
		xhr.open("POST", service, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4 && xhr.status === 200) {
	        	let json = JSON.parse(xhr.responseText);
	        	datastore[endpoint+"Data"] = json;
	        	if (endpoint !== 'test' && !checkError(json)){
	        		// console.log("assigning userData for "+endpoint)
	        		// console.log(json)
	        		// console.log("checkError: "+checkError(json))
	        		datastore["userData"] = json;
	        	}
	        	callback(json);
	    	}
		}
		let toSend = JSON.stringify(data);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.send(toSend);
	}

}