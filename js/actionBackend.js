/////// generic ///////
function checkError(data) {
	if (data === undefined || data['error'] || data["msg"] !== undefined) 
		return true; 
	else
		return false;
}

function changeFormSoft(data) {
	if (!checkError(data)) {
		let hasHeart = data['result']['has_heart'];
		window.user.changeForm(!hasHeart);
	}
}

function cleanScriptName(scriptname){//
	let noSpaces = scriptname.split(" ").join("_");
	let noSingles = noSpaces.split("'").join("");
	let noDoubles = noSingles.split('"').join("");
	if (scriptname.length === 0)
		noDoubles = '_'
	return noDoubles
}

function cleanScriptText(scripttext){
	if (scripttext.length === 0) scripttext = ' ';
	return scripttext
}

function clearLog(ele) {
	ele.innerHTML = '';
}


function loadingClear() {
	messageLog(actionModal.actionLoading, "");
}
function loadingLog() {
	let load = "<span style='color:darkgreen;'> loading... </span>";
	messageLog(actionModal.actionLoading, load);
}
function loadingReturn(html) {
	messageLog(actionModal.actionLoading, html);
}
function loadingOutputClear(){
	messageLog(actionModal.actionOutputLoading, "");
}

function loadingOutputLog (){
	let load = "<span style='color:#ff761a;'> running... </span>";
	messageLog(actionModal.actionOutputLoading, load);
}
function loadingOutputReturn(html) {
	messageLog(actionModal.actionOutputLoading, html);
}
function messageLog(ele, msg) {
	ele.innerHTML = msg;
}

function printError(display, data) {
	messageLog(display, data["msg"]);
}

function printUser(display, data) {
	let props = ["username", "id", "location",
				 "character", "form", "material",
				 "total_collected", "total_dropped"]

	let status = "healthy"
	if (data["user"]["form"] === "ghost")
		status = "ghost"

	for (let prop of props){
		let key = prop.replace("total_", "");
		let value = data["user"][prop];
		if (prop === "form") {
			key = "status";
			value = status;
		}
		returnLog(display, key +": "+value);
	}
}


function returnLog(ele, msg) {
	if (ele.innerHTML === '')
		ele.innerHTML = msg;
	else
		ele.innerHTML = ele.innerHTML + "<br>" + msg;
}

function reconnect(data) {
	datastore['error'] = data;
	// toolbar.showConnect();
	actionModal.showError()
}

////////// Noop Callback ///////////////////
function noopCallback(data) {
	if (checkError(data)){
		reconnect(data);
		return false;
	}
	else 
		return true;
}

////////// Output Callback /////////////////
function outputCallback(data, printMaterial) {
	if (checkError(data)){
		reconnect(data);
		return false;
	}
	else{
		let output = data['result']['output'];
		let hasHeart = data['result']['has_heart'];
		let material = data['result']['material'];
		let running = data['result']['running']
		let img = "<img src='images/icons/material.gif' width=10 height=15>"
		

		

		if (printMaterial)
			loadingReturn(material +" "+ img); // drop collect test || run
		// else
		// 	loadingClear();

		window.user.pingOutputSubscribers(data['result']); // Test this on goku civilian
		actionModal.updateUser(); //updates user
		actionModal.showOutput(output);
		toolbar.showStatus();

		if (running)
			loadingOutputReturn("<small style='color:#ff761a;'>script is long running</small>");
		else if (hasHeart)
			loadingOutputReturn("<small style='color:darkgreen;'>script ran safely</small>");
		else if (hasHeart !== null)
			loadingOutputReturn("<small style='color:#ff761a;'>heart file missing</small>");
		else
			loadingOutputReturn("<small style='color:darkred;'>script failed to run</small>");
		return true;
	}
}

function outputSend(method, callback) {
	actionModal.clearOutput();
	let actionScriptname = document.getElementById("actionScriptname");
	actionScriptname.value = cleanScriptName(actionScriptname.value);
	let actionScripttext = document.getElementById("actionScripttext");
	actionScripttext.value = cleanScriptText(actionScripttext.value);
	if (actionScripttext.value.length === 0) actionScripttext.value = ' '; // temporary fix. remove backend check
	// console.log('')
	// console.log('script name: <'+ actionScriptname.value+'>')
	// console.log('script text: <'+actionScripttext.value+'>')
	if (actionScriptname.value.length === 0 || actionScripttext.value.length === 0)
		actionModal.messageLog("<small style='color:darkred;'>script name and text needed</small>")
	else {
		actionModal.clearOutput();
		//actionModal.loadingOn();
		actionModal.loadingOutputOn();
		if (datastore['userData'])
			actionModal.appendOutputUserTitle(datastore['userData']);
		data = {filename: actionScriptname.value,
				filetext: actionScripttext.value,
				row: user.row, col: user.col, fileid: window.selectedID, location: user.location};
		datastore[method](callback, data)
	}
}

////////// Collect (collect file) //////////
function collectCallback(data) {
	changeFormSoft(data);
	if (data['result']) data['result']['method'] = 'collect'
	if (outputCallback(data, false)){
		let scriptid = data['result']['script']['id'];
		if (notGhost())
			collectScript(scriptid);
		actionModal.updateCollectButtons();
	}
}
function collectSend() {
	if (notGhost()){
		outputSend('collect', collectCallback);
	}
	else
		reconnect({msg: "you're a ghost", error: true})
}

////////// Drop (drop file) ////////////////
function dropCallback(data) {
	if (data['result']) data['result']['method'] = 'drop'
	if (outputCallback(data, true)){
		map.layScript(data);
		checkScript(data['result']['script']['id']);
	}
}
function dropSend() {outputSend('drop', dropCallback);}

///////// Delete (delete file) ////////////
function deleteCallback(data) {
	if (noopCallback(data)){
		map.removeObject(data['script_id']); // remove script - selected id
		loadingReturn("<small style='color:#A6B1E1;'>script removed</small>");
		loadingOutputReturn("<small style='color:#A6B1E1;'>deleted</small>");

		//checkScript(data['result']['script']['id']);
	}
}
function deleteSend() {outputSend('delete', deleteCallback);}

///////// Edit (edit file) ////////////
function editCallback(data) {
	if (data['result']) data['result']['method'] = 'edit'
	if(outputCallback(data, true)){
		map.editScript(data)
	}
} // do nothing more
function editSend() {outputSend('edit', editCallback);}

///////// Place (place file) ////////////
function placeCallback(data) {
	changeFormSoft(data);
	if (data['result']) data['result']['method'] = 'place'
	outputCallback(data, false);
}
function placeSend() {outputSend('place', placeCallback);}


////////// Run (Run scripts) ///////////
function runCallback(data) {
	changeFormSoft(data);
	if (data['result']) data['result']['method'] = 'run'
	outputCallback(data, false)
}
function runSend() {outputSend('run', runCallback);}

////////// Test (test script) ///////////
function testCallback(data) {
	if (data['result']) data['result']['method'] = 'test';
	outputCallback(data, true);
}
function testSend() 		{outputSend('test', testCallback);}

///////////////////////////////////////////////////////////////
////////// User Functions ////////////////
//////////////////////////////////////////////////////////////

/// load credentials
function loadCredentials() {
		datastore.loadCredentials(toolbar.connectUsername.value, toolbar.connectPassword.value);
}

function loadUsername(username) {
	datastore.loadCredentials(username, datastore['password']);
}


/// UserConsole Callback ///
function userCallback(data) {
	actionModal.loadingOff();
	if (checkError(data))
		reconnect(data)
	else{
		window.connected = true;
		window.user.pingOutputSubscribers(data['user']);
		actionModal.showHeal(data);
		window.map.checkScripts(); // hmm?
		toolbar.showStatus();
	}
}


////////// Create (New Player) ///////////
function createSend() {
	loadCredentials();
	actionModal.loadingOn();
	datastore.create(userCallback);
}

////////// New (New Container) ///////////
function newCallback(data) {
	if (data['user']) data['user']['method'] = 'new';
	userCallback(data);
}

function newSend() {
	loadCredentials();
	actionModal.loadingOn();
	datastore.full_restore(newCallback);
}

////////// Heal (Heal Container) ///////////
function healCallback(data) {
	if (data['user']) data['user']['method'] = 'heal';
	userCallback(data);
}

function healSend() {
	loadCredentials();
	actionModal.loadingOn();
	datastore.heal(healCallback);
}

////////// Touch (fetch username, character, and ip) ///////////
function touchCallback(data) {
	if (data['user']) data['user']['method'] = 'touch';
	userCallback(data);
}

function touchSend() {
	loadCredentials();
	actionModal.loadingOn();
	datastore.touch(touchCallback);
}

////////// Update (update username, character, and ip) ///////////
function updateFormUpdate(char) {
	document.getElementById("actionRadioInput").value = char;
}

function updateSend() {
	let charInput = document.getElementById("actionRadioInput").value;
	let nameInput = document.getElementById("actionNameInput").value;
	loadUsername(nameInput)

	if (charInput.lenth === 0 || nameInput.length === 0)
		actionModal.messageLog("<small style='color:darkred;'>character and name must be selected</small>")
	else {
		data = {character: charInput}
		datastore.update(userCallback, data)
	}
}

