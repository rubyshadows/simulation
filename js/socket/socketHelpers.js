///////////// socket responders /////////////
function arrival(userData){
	// make new character
	let newUser = userData['user'];
	let usid = userData['sid'];
	let form = newUser['form']

	if (usid === window.sid){
		// console.log("blocked my sid")
		return
	}
	else{
		window.characters[usid] = makeCharacter(map, userData['row'], userData['col'], newUser['character']);
		if (form === 'ghost')
			window.characters[usid].toGhost();
	}
}

function departure(sid) {

	let person = window.characters[sid]
	if (person != undefined)
		person.remove()
}

function ghostPlayer(playerSid) {
	if (playerSid !== window.sid){

		let person = window.characters[playerSid]
		if (person != undefined)
			person.toGhost()
	}
}

function loadEveryone(sid, people) {
	window.sid = sid;

	for (let person of people)
		arrival(person)
}

function movePlayer(data) {
	let playerSid = data['sid'];
	if (playerSid === window.sid){
		console.log("blocked my sid from moving")
		return
	}

	let row = data['row'];
	let col = data['col'];
	let player = window.characters[playerSid];
	if (player)
		player.go(row, col);
}

function unghostPlayer(playerSid) {
	if (playerSid !== window.sid){

		let person = window.characters[playerSid]
		if (person != undefined)
			person.unGhost()
	}
}

function updatePlayer(userData){
	let newUser = userData['user'];
	let usid = userData['sid'];
	let username = newUser['username']
	let character = newUser['character']
	let form = newUser['form'];
	if (usid === window.sid){
		return
	}
	else{
		window.characters[usid].username = username;
		window.characters[usid].character = character;
		if (form === 'ghost')
			window.characters[usid].toGhost();
		else
			window.characters[usid].unGhost();
		let row = window.characters[usid].row;
		let col = window.characters[usid].col;
		window.characters[usid].update(row, col);
	}
}

function recieveDrop(scriptData) {
	let playerSid = scriptData['sid'];
	if (playerSid !== window.sid){
		map.layScript(scriptData);
	}
}

///////////// socket listeners //////////
function initSockets() {
	window.characters = {}
	socket.on('connect', fetchEveryone);
	socket.on('arrival', arrival);
	socket.on('departure', departure);
	socket.on('movePlayer', movePlayer);
	socket.on('drop', recieveDrop);
	socket.on('update', updatePlayer);
	socket.on('ghost', ghostPlayer);
	socket.on('unghost', unghostPlayer);
	socket.on('talk', function() {});
}


///////////// socket callbacks //////////


///////////// socket emitters //////////
function fetchEveryone(){ // should load all characters
	socket.emit("fetch_everyone", {}, loadEveryone)
}

function requestArrival() {
	let newUser = datastore.userData
	socket.emit("request_arrival", newUser)
}

function requestDrop(scriptData) {
	console.log("requestDrop"); socket.emit("request_drop", scriptData)
}

function requestGhost() {socket.emit("request_ghost")}

function requestMove(row, col) {
	socket.emit("request_move", {row: row, col: col})
}

function requestUnghost() {socket.emit("request_unghost")}

function requestUpdate() {
	let newUser = datastore.userData
	socket.emit("request_update", newUser)
}