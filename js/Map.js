

class Map {
	constructor(mapWidth, mapHeight, mapRows, mapCols, floorTileImg) {
		window.map = this;
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.mapRows = mapRows;
		this.mapCols = mapCols;
		this.cellSize = mapWidth / mapCols;
		this.barrierList = {};
		this.items = {};
		this.floorTileImg = floorTileImg;
		this.mapDiv = document.getElementById("map");
		this.mapDiv.map = this;
		this.location = window.mapLocation;
		this.scripts = [];
		this.rawscripts = [];

		this.spawn = [3, 3];

		datastore.load(map.populateScripts, this.location);

	}

	populateScripts(data) {
		let scripts = data['scripts'];
		//map.rawscripts = scripts;
		for (let script of scripts){
			let sid = script['id'];
			let row = script['row'];
			let col = script['col'];
			let scriptObj = new ImageObject(sid, .8, .8, [], 
							"images/icons/bash.png", -4, "script", script);
			window.map.addObject(scriptObj, row, col);
		}
		initScriptCollectors()
	}

	drawSafepoints() {
		this.floorTileImg = "images/map/blueroomfloor.png"
		for (let point of this.safepoints){
			let tile = this.layTile(point[0], point[1],1.5);
			tile.style['z-index'] = 1000;
			tile.style['opacity'] = .3;
			tile.lastChild.style.borderRadius = "50px"
		}
		this.floorTileImg = "images/map/roomfloor.png"
	}
	drawBarriers() {
		this.floorTileImg = "images/map/redroomfloor.png"
		for (let i = 0; i < this.mapRows; i++)
			for (let j = 0; j < this.mapCols; j++)
				if(this.barrierList[i+"-"+j]){
					let tile = this.layTile(i, j, 1);
					tile.style['z-index'] = 1000;
					tile.style['opacity'] = .5;
				}
		this.floorTileImg = "images/map/roomfloor.png"
	}

	setTiles() {
		let size = 4;
		for (let i = 0; i < this.mapRows; i+=size)
			for (let j = 0; j < this.mapCols; j+=size)
				this.layTile(i, j, size);
	}

	layScript(data) {
		let script = data['result']["script"];
		script['user'] = data['user'];
		let sid = script['id'];
		let row = script['row'];
		let col = script['col'];
		let scriptObj = new ImageObject(sid, .8, .8, [], 
						"images/icons/bash.png", -1, "script", script);
		map.addObject(scriptObj, row, col);
	}

	layTile(row, col, size) {
		let tile = document.createElement("div"); 
		setValues(tile, col * this.cellSize, row * this.cellSize, 
				  this.cellSize * size, this.cellSize * size);
		tile.setAttribute("class", "tile"); 

		let tileImg = document.createElement("img"); 
		tileImg.setAttribute("src", this.floorTileImg);
		tile.append(tileImg);

		this.mapDiv.append(tile);
		return tile;
	}

	addObject(imgObj, row, col){
		let imgDiv = document.createElement("div");
		setValues(imgDiv, this.cellSize * col, this.cellSize * row, 
				  this.cellSize * imgObj.objWidth, 
				  this.cellSize * imgObj.objHeight);
		imgDiv.setAttribute("class", "object");
		imgDiv.setAttribute("id", imgObj.ID);
		imgDiv.style['z-index'] = Math.floor(row+imgObj.zOffset);

		let img = document.createElement("img");
		img.setAttribute("src", imgObj.img);
		imgDiv.append(img);

		imgObj.imgDiv = imgDiv;
		this.mapDiv.append(imgDiv);
		imgObj.row = row; imgObj.col = col;
		this.enter(imgObj);

		let data = imgObj.objData;
		if (imgObj.objType === "script") {//
			window.map.rawscripts.push(data) // difficult to remove after random delete
			// without removal, characters can't rely on rawscripts after load
			// also difficult to edit, characters can't read user scripts suceptible to edits
			// later make a rawscripts dict and replace data accordingly
			this.scripts.push(imgObj);
			imgDiv.setAttribute("data-toggle", "modal");
			imgDiv.setAttribute("data-target", "#actionModal");
			imgDiv.setAttribute("data-script", JSON.stringify(data));
			imgDiv.addEventListener('click', actionModal.showCollect);
		}
		
	}

	editScript(data) {
		// get imgObj - get div - set attributes
		// does not edit rawscripts for now
		let imgObj = map.find(data['result']['script']['id']);
		let imgDiv = document.getElementById(imgObj.ID);
		imgDiv.setAttribute("data-script", JSON.stringify(data['result']['script']));

	}

	checkScripts() {
		for (let imgObj of this.scripts)
			checkScript(imgObj.ID) // after you delete one, this will break
	}

	getItems(row, col){
		let itemDict = this.items[Math.floor(row)+"-"+Math.floor(col)];
		let res = [];
		for (let item in itemDict) {
			if (!itemDict.hasOwnProperty(item)) continue;
			if (itemDict[item])
				res.push(itemDict[item]);
		}
		return res;
	}

	removeObject(ID) {
		let imgObj = this.find(ID);
		this.leave(imgObj);
		let img = document.getElementById(imgObj.ID);
		this.mapDiv.removeChild(img);
		imgObj.imgDiv = undefined;
		// leave the space
		// remove the image from the dom
	}

	find(ID){
		return this.items[ID];
	}

	initCell(row, col){
		if (this.items[Math.floor(row)+'-'+Math.floor(col)] === undefined)
			this.items[Math.floor(row)+'-'+Math.floor(col)] = {};
	}

	leave(ele){
		this.items[Math.floor(ele.row)+'-'+Math.floor(ele.col)][ele.ID] = undefined;
		this.items[ele.ID] = undefined;
		if (ele.constructor.name === "ImageObject") 
			for (let barrier of ele.barrierList)
					this.barrierList[(barrier[0]+row) + "-" + (barrier[1]+col)] = false;
	}

	enter(ele){
		this.initCell(Math.floor(ele.row), Math.floor(ele.col))
		this.items[Math.floor(ele.row)+'-'+Math.floor(ele.col)][ele.ID] = ele;
		this.items[ele.ID] = ele;
		if (ele.constructor.name === "ImageObject")
			for (let barrier of ele.barrierList)
					this.barrierList[Math.floor(barrier[0]+ele.row) + "-" + Math.floor(barrier[1]+ele.col)] = true;

	}

	set(width, height, rows, cols, spawn) {
		// set map's height and width manually
		this.spawn = spawn;
		this.mapWidth = width;
		this.mapHeight = height;
		this.mapRows = rows;
		this.mapCols = cols;
		this.cellSize = this.mapHeight / this.mapCols;
	}
}

class ImageObject{
	constructor(ID, objWidth, objHeight, barrierList,
				img, zOffset, objType, data){
		this.ID = ID;
		this.objWidth = objWidth;
		this.objHeight = objHeight;
		this.barrierList = barrierList;
		this.img = img;
		this.row = '';
		this.col = '';
		this.zOffset = zOffset;
		this.objType = objType;
		this.objData = data;
	}
}

function emptyWood(){
	let m = {mapHeight: 2800, 
			 mapWidth: 900, 
			 mapRows: 140, 
			 mapCols: 45,
			 floorTileImg: "images/map/roomfloor.png"};
	let map = new Map(m.mapWidth, m.mapHeight, m.mapRows,
					  m.mapCols, m.floorTileImg);
	map.setTiles();
	return map;
}

function emptyUnset() {
	let m = {mapHeight: 1200, 
			 mapWidth: 1200, 
			 mapRows: 40, 
			 mapCols: 40,
			 cellSize: 1200 / 40,
			 floorTileImg: "images/pixels/black-1-1.png"};
	let map = new Map(m.mapWidth, m.mapHeight, m.mapRows,
					  m.mapCols, m.floorTileImg);
	return map;
}

function empty(){
	let m = {mapHeight: 2400, 
			 mapWidth: 1200, 
			 mapRows: 80, 
			 mapCols: 40,
			 cellSize: 1200 / 40,
			 floorTileImg: "images/pixels/black-1-1.png"};
	let map = new Map(m.mapWidth, m.mapHeight, m.mapRows,
					  m.mapCols, m.floorTileImg);
	map.setTiles();
	return map;
}

function makeSeaHorse(map) {

	let box = new ImageObject("box_0", 1, 1, [], "images/map/cabinwallTop.png", -1,
								"tile", {});
	
	let u = 'up', d = 'down', r = 'right', l = 'left', 
		ul = 'upleft', ur = 'upright', dl = "downleft", dr = "downright";
	let drawlist = [r,r,r,u,u,r,ur,u,r,r,d,d,l,d,d,r,d,d,r];


	drawlist = [r,u,r,u,r,r,u,u,r,r,r,d,l,d,r,d,r,
				d,r,d,r,r,d,l,l,d,l,d,l,d,r,d,l,d,l,d,l,d,d,r,d,d,d,
				l,d,l,l,l,u,u,u,r,r,d,l,d,r,u,r,u,l,u,u,l,
				u,l,u,l,u,l,u,l,u,u,r,u,r,u,r,r,r,u,l,u]


	let cursor = [10, 5];
	let count = 0;
	map.addObject(box, cursor[0], cursor[1]);
	for (let stroke of drawlist){
		for (let i = 0; i < 2; i++){
			switch(stroke){
				case u: cursor[0] -= 1; break;
				case d: cursor[0] += 1; break;
				case r: cursor[1] += 1; break;
				case l: cursor[1] -= 1; break;
				case ur: cursor[0] -= 1; cursor[1] += 1;
				case ul: cursor[0] -= 1; cursor[1] -= 1;
				case dr: cursor[0] += 1; cursor[1] += 1;
				case dl: cursor[0] += 1; cursor[1] -= 1;
			}
			count++;
			//box = new ImageObject("box_"+count,1, 1, [[0,0]], "images/map/material/box.png")
			map.addObject(box, cursor[0], cursor[1]);
		}
		
	}


}