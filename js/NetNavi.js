class NetNavi extends Civilian {
	// never dies. because you can't move as a ghost
	// always heals you.
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList)

		this.species = 'collectors'

		// this.minRange = 2;
		this.maxRange = 15;
		this.locked = false;
		this.silent = false;
		this.began = false;
		this.cutsceneMode = false;
		this.arrived = false;
		this.completed = false;
		this.delayOpen = 2000;
		this.isNavi = true;

		this.forceOpen();//
	}

	checkSilence(){if (this.silent) {this.flavorText = ""; this.forceClose(); return true;} return false;}

	markScript(npcScript, color) {npcScript.appendColorTile(color);}
	unmarkScript(npcScript, color){npcScript.removeColorTile();}

	panic() {
		// mark all enemy scripts,
		// then run to heart file generically
	}

	process(char) { // would disable this in cutscene mode
		if(this.debugNavi) console.log("process start - done: "+this.done)
		if (this.silent) this.forceClose();
		if (this.locked || this.cutsceneMode) return; // if locked, don't get interrupted
		let script = this.scriptList[this.idx];
		if (!this.began){
			this.began = true;
			this.getTargetScript();
			cutScene(this, script.messages, this.baseSpeed, script.path);
			return;
		}

		//arrival removes cutscenemode

		//if (this.locked && !this.closed) this.forceOpen(); // updates it. seems error prone
		
		// if (!this.done || this.interrupted){ // maybe
		// 	super.process(char);
		// }
		
		// if navi is locked, this should not be reached
		// when not locked, it closes when you're close. doesn't do super

		// assuming we're not in a cutscene mode, we can talk even if not arrived
		// this is an always talking tint, alice may have a talk less disposition
		this.minRangeRetract();
		if (this.arrived) {
			if(this.debugNavi) console.log("process arrived - done: "+this.done)
			//console.log('netNavi.process, arrived true - script #: '+this.idx)
			if (this.inRange(window.user)){
				//console.log('netNavi.process, arrived true - closing - script #: '+this.idx)
				this.forceClose(); 
			}
			else{
				if (script.arrivedText){
					//console.log('netNavi.process, arrived true - opening - script #: '+this.idx)
					this.flavorText = userInject(this.currentScript().arrivedText);
					this.softOpen(); 
				}
			}
		}
		 

	}

	processOutput(result) {

		let resultScript = result['script'];
		if(this.debugNavi) console.log("before super.processOutput - done: "+this.done)
		super.processOutput(result);
		if(this.debugNavi) console.log("after super.processOutput - done: "+this.done)

		let script = this.scriptList[this.idx];
		if (script.activatedCondition(this, result)){ 
			this.arrived = false;
			this.next();
			script = this.scriptList[this.idx];
			this.pathSearch = makeNaviSearch(this);
			this.getTargetScript(); // here
			if(this.debugNaviSearch) console.log("in processOutput - selectedRow: "+this.selectedRow +" - selectedCol: "+this.selectedCol)
			if (!this.silent)
				cutScene(this, script.messages, this.baseSpeed, script.path) // hm
		} 

	}

	npcUpdate(char) {
		//if (!this.done) this.arrived = false; you can move and have arrived
		if(this.silent) {this.forceClose(); return;}
		if (this.locked) {
			this.softOpen(); // if you're locked you talk. i guess cutscene mode is locked+; using timed popovers
			return;
		}

		if (this.cutsceneMode && !this.arrived && !this.closed) this.softOpen();

		if (this.cutsceneMode) return;

		// if (distance(this.row, script.row, this.col, script.col) < 2){
		// 	if (!this.arrived){
		// 		console.log("navi arrived")
		// 		this.currentScript().arrivedFunction();
		// 	}

		// 	this.arrived = true;
		// }

		if (this.arrived) { // so if we're moving after having arrived
			this.flavorText = userInject(this.currentScript().arrivedText);
			this.process(char); // both process and update call this
		}

		// but how could we be done from far away
		// if ((!this.done || this.interrupted) || (distance(this.row, script.row, this.col, script.col) > 3)) // if done and far dont speak
		// 	this.process(char); // may change this for cutscenes. more useful for coop play than for straight interactive cutscenes
		// else{
		// 	if (!this.arrived) {
		// 		this.arrived = true;
		// 		this.currentScript().arrivedFunction(this);
		// 	}
		// 	this.flavorText = userInject(this.currentScript().arrivedText); // do arrival heart function
		// 	this.checkSilence();
		// 	this.locked = false; // not locked on arrival. Would be cool to
		// 	console.log('update arrival')
		// 	this.softOpen(); 
		// }
	}

	 //move while unlocked.
	 	// forceOpen or forceClose on process.

	updateFlavorText(){
		if (!this.done || this.interrupted)
			this.flavorText = userInject(this.currentScript().walkingText);
		this.checkSilence();
	}
}

class Alice extends Civilian {
	// never dies. because you can't move as a ghost
	// always heals you.
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList)

		this.species = 'collectors';

	}

	civilianProcess() {super.process();}
	naviProcess(){
		if (this.silent && !this.closed) this.forceClose();
		if (this.locked || this.cutsceneMode) return;
		this.minRangeRetract();
		if (this.inRange(window.user) && this.arrived){
			if (!this.closed)
				this.forceClose(); 
		}
		else{
			if (script.arrivedText && this.arrived){
				this.flavorText = userInject(this.currentScript().arrivedText);
				//console.log('process - arrived: '+this.arrived)
				if (this.arrived){
					//console.log('process arrival')
					this.forceOpen(); // not firing
				}
			}
		}
	}
	critterspeed(){this.baseSpeed = CRITTERSPEED();}
	truespeed(){this.baseSpeed = ALICESPEED();}
	alicespeed(){this.baseSpeed = ALICESPEED();}

	// follow, stayopen, face, and forceopen

	// alice needs the perfect follow *shivers*
	// it needs to get to your general area first.
	// doesn't call again until you're outside it's range
	// not a super close follow - variable range
}

function getAlice() {return window.characterDict['alice'][0];}
function getJu() {return window.characterDict['ju'][0];}