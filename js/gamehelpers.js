function notGhost() {
	return window.user.form != "ghost"
}

function author(scriptid) {
	let imgDiv = document.getElementById(scriptid);
	let data = JSON.parse(imgDiv.getAttribute("data-script"));
	return data['user']['username'] == window.user.username
}

function collectedList(scriptid) {
	let imgDiv = document.getElementById(scriptid);
	let data = JSON.parse(imgDiv.getAttribute("data-script"));
	return data['collected_list'];
}

function collectScript(scriptid) {
	let imgDiv = document.getElementById(scriptid);
	let data = JSON.parse(imgDiv.getAttribute("data-script"));
	data['collected_list'][window.user.ID] = true;
	imgDiv.setAttribute("data-script", JSON.stringify(data));
	checkScript(scriptid)
}//

function searchList(collectedList) {
	if (collectedList[window.user.ID])
		return true;
	return false;
}

function checkScript(scriptid) {
	/// turns a script transparent if collected
	let imgDiv = document.getElementById(scriptid);
	if (!imgDiv) return;
	let data = JSON.parse(imgDiv.getAttribute("data-script"));
	if (searchList(collectedList(scriptid)) || author(scriptid)){
		imgDiv.style.opacity = '0.5';
		return true
	}
	else{
		imgDiv.style.opacity = '1';
		return false
	}
}

function checkObject(obj) {
	if (obj.constructor.name == "Character")
		return "character"
	else if (obj.objType === "script")
		return "script"
	else
		return "object"
}