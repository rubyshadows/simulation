class Builder {
	// Builds advanced pieces of map, such as walls
	// This may be the wood type builder
	constructor(map, wallHeight) {
		window.builder = this;
		this.map = map;
		this.tops = "images/map/walls/top/";
		this.backs = "images/map/walls/back/"
		this.theme = "brown/";
		this.wallHeight = wallHeight

		this.multiplier = 0.5;
		this.widthLong = 3 * this.multiplier;
		this.widthShort = 1 * this.multiplier;
		this.heightLong = 3 * this.multiplier;
		this.heightShort = 1 * this.multiplier;
		this.cell = 1 * this.multiplier;

		this.wallShrink = true;
		this.darkSide = "inside"; // only matters for 2nd part of algorithm
	}

	buildWalls(cursor, drawlist){
		/*
			rhtl - right horizontal top left

		*/
		let widthLong = this.widthLong;
		let widthShort = this.widthShort;
		let heightLong = this.heightLong;
		let heightShort = this.heightShort;
		let cell = this.cell;
		let zoffset = 4;

		let rhtl = new ImageObject("rhtl", widthLong, heightShort, [[0,0],[0,1],[0,2]], this.tops+this.theme+"rhtl-1-3.png", zoffset, "top-t", {});
		let rht = new ImageObject("rht", widthLong, heightShort, [[0,0],[0,1],[0,2]], this.tops+this.theme+"rht-1-3.png", zoffset, "top-t", {});
		let dhtr = new ImageObject("dhtr", widthLong, heightShort, [[0,0],[0,1]], this.tops+this.theme+"dhtr-1-3.png", zoffset, "top-t", {});
		
		let dvrt = new ImageObject("dvrt", widthShort, heightLong, [[0,0],[1,0],[2,0]], this.tops+this.theme+"dvrt-3-1.png", zoffset, "top-v", {});
		let dvr = new ImageObject("dvr", widthShort, heightLong, [[0,0],[1,0],[2,0]], this.tops+this.theme+"dvr-3-1.png", zoffset, "top-v", {});
		let lvrb = new ImageObject("lvrb", widthShort, heightLong, [[0,0],[1,0]], this.tops+this.theme+"lvrb-3-1.png", zoffset, "top-v", {});

		let lhbr = new ImageObject("lhbr", widthLong, heightShort, [[0,0],[0,1],[0,2]], this.tops+this.theme+"lhbr-1-3.png", zoffset, "top-b", {});
		let lhb = new ImageObject("lhb", widthLong, heightShort, [[0,0],[0,1],[0,2]], this.tops+this.theme+"lhb-1-3.png", zoffset, "top-b", {});
		let uhbl = new ImageObject("uhbl", widthLong, heightShort, [[0,0],[0,1],[0,2]], this.tops+this.theme+"uhbl-1-3.png", zoffset, "top-b", {});

		let uvlb = new ImageObject("uvlb", widthShort, heightLong, [[0,0],[1,0]], this.tops+this.theme+"uvlb-3-1.png", zoffset, "top-v", {});
		let uvl = new ImageObject("uvl", widthShort, heightLong, [[0,0],[1,0],[2,0]], this.tops+this.theme+"uvl-3-1.png", zoffset, "top-v", {});
		let rvlt = new ImageObject("rvlt", widthShort, heightLong, [[0,0],[1,0],[2,0]], this.tops+this.theme+"rvlt-3-1.png", zoffset, "top-v", {});

		// all back pieces are row:4 col:3 // multiplied by mulitplier
			// 12 barriers // little barriers
		let backBarrier = [[0,0],[0,1],[0,2],
						   [1,0],[1,1],[1,2],
						   [2,0],[2,1],[2,2],
						   [3,0],[3,1],[3,2]
						  ]
		for (let i = 0; i < backBarrier.length; i++){
			backBarrier[i][0] = backBarrier[i][0] * window.builder.multiplier;
			backBarrier[i][1] = backBarrier[i][1] * window.builder.multiplier;
		}
		let backHeight = widthLong + (widthLong/3);

		let btl = new ImageObject("btl", widthLong, backHeight, backBarrier, this.backs+this.theme+"btl-4-3.png", -1, "back", {});
		let bt = new ImageObject("bt", widthLong, backHeight, backBarrier, this.backs+this.theme+"bt-4-3.png", -1, "back", {});
		let btr = new ImageObject("btr", widthLong, backHeight, backBarrier, this.backs+this.theme+"btr-4-3.png", -1, "back", {});
		let btrSm = new ImageObject("btr", widthLong-cell, backHeight, backBarrier, this.backs+this.theme+"btr-4-3.png", -1, "back", {});

		let bml = new ImageObject("bml", widthLong, backHeight, backBarrier, this.backs+this.theme+"bml-4-3.png", -1, "back", {});
		let bm = new ImageObject("bm", widthLong, backHeight, backBarrier, this.backs+this.theme+"bm-4-3.png", -1, "back", {});
		let bmr = new ImageObject("bmr", widthLong, backHeight, backBarrier, this.backs+this.theme+"bmr-4-3.png", -1, "back", {});
		let bmrSm = new ImageObject("bmr", widthLong-cell, backHeight, backBarrier, this.backs+this.theme+"bmr-4-3.png", -1, "back", {});

		let bbl = new ImageObject("bbl", widthLong, backHeight, backBarrier, this.backs+this.theme+"bbl-4-3.png", -1, "back", {});
		let bb = new ImageObject("bb", widthLong, backHeight, backBarrier, this.backs+this.theme+"bb-4-3.png", -1, "back", {});
		let bbr = new ImageObject("bbr", widthLong, backHeight, backBarrier, this.backs+this.theme+"bbr-4-3.png", -1, "back", {});
		let bbrSm = new ImageObject("bbr", widthLong-cell, backHeight, backBarrier, this.backs+this.theme+"bbr-4-3.png", -1, "back", {});



		function addBack(piece, cursor, major, minor) {
			if (minor)
				return
			let obj1, obj2, obj3;
			switch(piece) {
				case "rhtl":
					obj1 = btl;
					obj2 = bml; //obj two gets added multiple times
					obj3 = bbl;
					break; 
				case "rht":
					obj1 = bt;
					obj2 = bm; //obj two gets added multiple times
					obj3 = bb;
					break; 
				case "dhtr":
					obj1 = btr; 
					obj2 = bmr;  //obj two gets added multiple times
					obj3 = bbr; 
					break; 
				default:
					return;
			}
			if (major){
				cell = window.builder.cell;
				if (piece === "dhtr"){
					obj1 = btrSm
					obj2 = bmrSm
					obj3 = bbrSm
				}
			}
			else 
				cell = 0

			map.addObject(obj1, cursor[0] + heightShort, cursor[1] + cell);
			let i = 1; // at i 0, middle and bottom cover top. for now this is best
			for (i = 1; i < window.builder.wallHeight; i++)
				map.addObject(obj2, cursor[0] + heightShort + (i * backHeight), cursor[1] + cell);
			map.addObject(obj3, cursor[0] + heightShort + (i * backHeight), cursor[1] + cell);

		}

		for (let stroke of drawlist){
			// stroke = {piece, dir}   dir = (row|col, pos|neg)
			let obj;
	
			switch(stroke['piece']){
				case "rhtl":  obj = rhtl;  break; // btl bml bbl //  locked
				case "rht":   obj = rht;   break; // bt bm bb // locked <- multiplied by wallHeight - 2
				case "dhtr":  obj = dhtr;  break; // btr bmr bbr // locked
				case "dvrt":  obj = dvrt;  break;
				case "dvr":   obj = dvr;   break;
				case "lvrb":  obj = lvrb;  break;
				case "lhbr":  obj = lhbr;  break;
				case "lhb":   obj = lhb;   break;
				case "uhbl":  obj = uhbl;  break;
				case "uvlb":  obj = uvlb;  break;
				case "uvl":   obj = uvl;   break;
				case "rvlt":  obj = rvlt;  break;
			}
			map.addObject(obj, cursor[0], cursor[1]);
			addBack(stroke['piece'], cursor, stroke['major'], stroke['minor'])
			let row = stroke["dir"][0];
			let col = stroke["dir"][1];
			cursor[0] += row;
			cursor[1] += col;
		}

	}


	test() {
		//creates a simple square room
		//this.map.set(500, 500,  20, 20, [8, 3])

		let widthLong = this.widthLong;
		let widthShort = this.widthShort;
		let heightLong = this.heightLong;
		let heightShort = this.heightShort;
		let cell = this.cell;
		// [0] row distance
		// [1] col distance
		let down = [heightLong, 0],
			right = [0, widthLong],
			up = [-heightLong, 0],
			left = [0, -widthLong];
		
///////////////// square basics /////////////////////////////
		let rhtl = {piece: "rhtl", dir: right}; // right top _ left corner 
		let rht = {piece: "rht", dir: right};

		let dhtr = {piece: "dhtr", dir: [0, cell*3]}; //top right corner

		let dvrt = {piece: "dvr", dir: down};
		let dvr = {piece: "dvr", dir: down};
		let lvrb = {piece: "lvrb", dir: [cell*3, -(cell*2)]}; 

		let lhbr = {piece: "lhbr", dir: left}; // bottom right corner
		let lhb = {piece: "lhb", dir: left};
		let uhbl = {piece: "uhbl", dir: [-(cell*2), 0]}; // bottom left corner

		let uvlb = {piece: "uvlb", dir: up};
		let uvl = {piece: "uvl", dir: up};
		let rvlt = {piece: "rvlt", dir: [0, 0]}; 
////////////////////////////////////////////////////////////////

///////////////// square advanced /////////////////////////////
		let rvrb = {piece: "lvrb", dir: [cell*2, 0]};
		let uhtr = {piece: "dhtr", dir: [-(cell*2), (cell*2)]};
		let lvlt = {piece: "rvlt", dir: [0, -(cell*3)]}; 

		let rhtlMjr = {piece: "rhtl", dir: right, major: true};
		let rhtMjr = {piece: "rht", dir: right, major: true};
		let dhtrMjr = {piece: "dhtr", dir: [0, cell*3], major: true};
		let dhtlMjr = {piece: "rhtl", dir: [cell, 0], major: true};

		let lhtr = {piece: "dhtr", dir: [0, -(cell*3)]};
		let lht = {piece: "rht", dir: left};
		let lhtrMjr = {piece: "dhtr", dir: [0, -(cell*3)], major: true};
		let lhtMjr = {piece: "rht", dir: left, major: true};

		let uhtl = {piece: "rhtl", dir: [-(cell *3), 0]};
		let uhtlMjr = {piece: "rhtl", dir: [-(cell *3), 0], major: true};

///////////////// square minor /////////////////////////////
		let rhtlMnr = {piece: "rhtl", dir: right, minor: true};

		let lhtrMnr = {piece: "dhtr", dir: [0, -(cell*3)], minor: true};;
		let lhtMnr = {piece: "rht", dir: left, minor: true};
		let dhtlMnr = {piece: "rhtl", dir: [cell, 0], minor: true};

		let uhtlMnr = {piece: "rhtl", dir: [-(cell *3), 0], minor: true};
////////////////////////////////////////////////////////////////
/////////////////// patches ////////////////////////////////////
		let dhtrp = {piece: "dhtr", dir: [0, cell*4]};
////////////////////////////////////////////////////////////////

		// start of cabin map function

		let cursor = [0, 0];

		let drawlist = [rhtlMjr]; // top left corner 
		for (let i = 0; i < 14; i++) 
			drawlist.push(rhtMjr); // go right 1
		drawlist.push(dhtrMjr); //top right corner

		for (let i = 0; i < 6; i++) 
			drawlist.push(dvr); // go down

		//////////////// advanced /////////////////
		drawlist.push(rvrb);
		drawlist.push(rhtl);
		for (let i = 0; i < 0; i++) 
			drawlist.push(rht); 
		drawlist.push(uhtr); // go right 2

		drawlist.push(uvlb);
		for (let i = 1; i < 6; i++)
			drawlist.push(uvl); // go up
		drawlist.push(rvlt);

		drawlist.push(rhtlMjr);
		for (let i = 0; i < 6; i++) 
			drawlist.push(rhtMjr); // go right 3
		drawlist.push(dhtrMjr);

		for (let i = 0; i < 22; i++) 
			drawlist.push(dvr); // go down
		drawlist.push(lvrb);

		drawlist.push(lhtrMnr);
		for (let i = 0; i < 6; i++)
			drawlist.push(lhtMnr); // go left 4
		drawlist.push(uhtlMnr);

		drawlist.push(uvlb)
		for (let i = 0; i < 6; i++)
			drawlist.push(uvl); // go up

		//////////////// super advanced ////////
		drawlist.push(lvlt) // should go directly left

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 0; i++)
			drawlist.push(lhtMnr); // go left 5 minor
		drawlist.push(dhtlMnr)

		for (let i = 0; i < 7; i++) 
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 3; i++)
			drawlist.push(lhtMnr); // go left 6
		drawlist.push(dhtlMnr)

		drawlist.push(dvrt)
		drawlist.push(rvrb)

		drawlist.push(rhtl)
		for (let i = 0; i < 13; i++)
			drawlist.push(rht) // go right 7
		drawlist.push(dhtr)

		drawlist.push(dvrt)
		for (let i = 0; i < 11; i++)
			drawlist.push(dvr);
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 8; i++)
			drawlist.push(lhtMnr);
		drawlist.push(dhtlMnr)

		for (let i = 0; i < 0; i++) 
			drawlist.push(dvr); // go down
		drawlist.push(rvrb)

		drawlist.push(rhtl)
		for (let i = 0; i < 7; i++)
			drawlist.push(rht) // go right 8
		drawlist.push(dhtr)

		drawlist.push(dvrt)
		for (let i = 0; i < 24; i++)
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 16; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(dhtlMnr)

		drawlist.push(dvrt) // go down
		drawlist.push(rvrb)
		
		drawlist.push(rhtl)
		for (let i = 0; i < 8; i++)
			drawlist.push(rht) // go right 9
		drawlist.push(dhtr)

		drawlist.push(dvrt)
		for (let i = 0; i < 11; i++)
			drawlist.push(dvr); // go down
		drawlist.push(rvrb)

		drawlist.push(rhtl)
		for (let i = 0; i < 0; i++)
			drawlist.push(rht) // go right 
		drawlist.push(uhtr)

		drawlist.push(uvlb);
		for (let i = 1; i < 12; i++)
			drawlist.push(uvl); // go up
		drawlist.push(rvlt);

		drawlist.push(rhtlMjr)
		for (let i = 0; i < 4; i++)
			drawlist.push(rhtMjr) // go right 
		drawlist.push(dhtrMjr)

		drawlist.push(dvrt)
		for (let i = 0; i < 23; i++)
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

/////////////////////////////////////////////
//////// second half from bottom right //////
/////////////////////////////////////////////


		drawlist.push(lhtrMnr)
		for (let i = 0; i < 4; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(uhtlMnr)

		drawlist.push(uvlb);
		for (let i = 1; i < 4; i++)
			drawlist.push(uvl); // go up
		drawlist.push(lvlt);

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 0; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(dhtlMnr)

		drawlist.push(dvrt)
		for (let i = 0; i < 3; i++)
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 16; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(uhtlMnr)

		drawlist.push(uvlb);
		for (let i = 1; i < 36; i++)
			drawlist.push(uvl); // go up
		drawlist.push(rvlt);

		drawlist.push(rhtlMjr)
		for (let i = 0; i < 9; i++)
			drawlist.push(rhtMjr) // go right 1
		drawlist.push(uhtr)

		drawlist.push(uvlb);
		for (let i = 1; i < 6; i++)
			drawlist.push(uvl); // go up
		drawlist.push(lvlt);

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 0; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(dhtlMnr)

		drawlist.push(dvrt)
		for (let i = 0; i < 3; i++)
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 7; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(uhtlMnr)

		drawlist.push(uvlb);
		for (let i = 1; i < 14; i++)
			drawlist.push(uvl); // go up
		drawlist.push(rvlt);

		drawlist.push(rhtlMjr)
		for (let i = 0; i < 9; i++)
			drawlist.push(rhtMjr) // go right 2
		drawlist.push(uhtr)

		drawlist.push(uvlb);
		for (let i = 0; i < 3; i++)
			drawlist.push(uvl); // go up
		drawlist.push(lvlt);

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 0; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(dhtlMnr)

		drawlist.push(dvrt)
		for (let i = 0; i < 1; i++)
			drawlist.push(dvr); // go down
		drawlist.push(lvrb)

		drawlist.push(lhtrMnr)
		for (let i = 0; i < 7; i++)
			drawlist.push(lhtMnr); // go left 
		drawlist.push(uhtlMnr)

		drawlist.push(uvlb);
		for (let i = 0; i < 20; i++)
			drawlist.push(uvl); // go up
		drawlist.push(rvlt);

		drawlist.push(rhtlMjr)
		for (let i = 0; i < 9; i++)
			drawlist.push(rhtMjr) // go right 2
		drawlist.push(uhtr)




		this.buildWalls(cursor, drawlist);
	}
}