//////// constants ///////////
function MAXDEPTH(){return 2000;}
function MAXDISTANCE() {return 10000;}
function CIVILIANSPEED() {return 10000;}
function USERSPEED() {return 350;}
function BOSSSPEED() {return CIVILIANSPEED() / 4;}
function NAVISPEED() {return CIVILIANSPEED() / 10;}
function CRITTERSPEED() {return CIVILIANSPEED() / 5;}
function GHOSTSPEED() {return CIVILIANSPEED();}
function ALICESPEED() {return NAVISPEED() / 2;}



function FULLSEARCH() {return "wallSearch";}
/////////////////////////////

function euclideanFace(row, col, selectedRow, selectedCol) {
	let left = [distance(row, selectedRow, col-1, selectedCol), "Left"];
	let right = [distance(row, selectedRow, col+1, selectedCol), "Right"];
	let down = [distance(row+1, selectedRow, col, selectedCol), "Down"];
	let up = [distance(row-1, selectedRow, col, selectedCol), "Up"];

	let directions = [left, right, down, up];
	directions.sort(function(a, b){
		return a[0] - b[0];
	});
	return directions[0][1];
}

function euclideanOpposite(row, col, selectedRow, selectedCol) {
	switch(euclideanFace(row, col, selectedRow, selectedCol)){
		case "Left": return "Right"; break;
		case "Down": return "Up"; break;
		case "Right": return "Left"; break;
		case "Up": return "Down"; break;
	}
}

function face(row, col, selectedRow, selectedCol) {
	/// may need to use euclidean distance to decide face to fix some wallSearch bugs
		if (selectedRow > row) 	return "Down";
		else if (selectedRow < row) return "Up";
		else if (selectedCol > col) return "Right";
		else if (selectedCol < col) return "Left";
		else return "Down";
}

function getPath (row, col, selectedRow, selectedCol, notOptimal) {
	if (!check(window.map, selectedRow, selectedCol))
		return [];
	let path = wallSearch(row, col, selectedRow, selectedCol);
	let safePath = safeSearch(row, col, selectedRow, selectedCol);
	if (path.length === 0)
		return safePath // wallsearch returning failure
	if(path.length > safePath.length && !notOptimal)
		return safePath
	return path;
}

function greaterWallSearch(row, col, selectedRow, selectedCol) {
	return getPath(row, col, selectedRow, selectedCol, true);
}

class Position {
		constructor (char, row, col, dir, depthLife) {
			this.char = char;
			this.selectedRow = char.selectedRow;
			this.selectedCol = char.selectedCol;
			this.row = row;
			this.col = col;
			this.dir = dir.toLowerCase();
			this.depthLife = depthLife;
			this.distance = distance(this.row, this.selectedRow, this.col, this.selectedCol);
			this.found = this.distance == 0;
			this.hitWall = !check(window.map, this.forwardCoordinate().row, this.forwardCoordinate().col);
			this.isWall = !check(window.map, this.row, this.col);
			if(this.isWall){
				this.distance = MAXDISTANCE(); // infinite
				this.found = false;
				//console.log(this.dir + " is wall");
			}

			
			//console.log("create position: "+ this.row + " - " + this.col + " - dist: "+this.distance)
			//console.log("dir - "+dir+" row: "+this.row + " - col: "+this.col);
			//console.log("distance: "+this.distance);
			//console.log("found: "+this.found);
		}

		forward() {
			switch(this.dir) {
				case "up": return this.newPos("up");
				case "down": return this.newPos("down");
				case "left": return this.newPos("left"); 
				case "right": return this.newPos("right");
			}
		}

		forwardCoordinate() {
			switch(this.dir) {
				case "up": return {row: this.row-1, col: this.col};
				case "down": return {row: this.row+1, col: this.col};
				case "left": return {row: this.row, col: this.col-1}; 
				case "right": return {row: this.row, col: this.col+1};
			}
		}

		goBack(){
			switch(this.dir) {
				case "up": return new Position(this.char, this.row + 1, this.col, this.dir, this.depthLife + 1);
				case "down": return new Position(this.char, this.row - 1, this.col, this.dir, this.depthLife + 1);
				case "left": return new Position(this.char, this.row, this.col + 1, this.dir, this.depthLife + 1); 
				case "right": return new Position(this.char, this.row, this.col - 1, this.dir, this.depthLife + 1);
			}
		}

		back() {
			switch(this.dir) {
				case "up": return this.newPos("down");
				case "down": return this.newPos("up");
				case "left": return this.newPos("right"); 
				case "right": return this.newPos("left");
			}
		}

		left() {
			switch(this.dir) {
				case "up": return this.newPos("left");
				case "down": return this.newPos("right");
				case "left": return this.newPos("down"); 
				case "right": return this.newPos("up");
			}
		}

		right() {
			switch(this.dir) {
				case "up": return this.newPos("right");
				case "down": return this.newPos("left");
				case "left": return this.newPos("up"); 
				case "right": return this.newPos("down");
			}
		}

		newPos(dir) {
			let urow = this.row - 1, ucol = this.col,
			drow = this.row + 1, dcol = this.col, 
			lrow = this.row, lcol = this.col - 1, 
			rrow = this.row, rcol = this.col + 1;
			switch (dir.toLowerCase()) {
				case "up": return (new Position(this.char, urow, ucol, dir, this.depthLife - 1));
				case "down": return (new Position(this.char, drow, dcol, dir, this.depthLife - 1));
				case "left": return (new Position(this.char, lrow, lcol, dir, this.depthLife - 1));
				case "right": return (new Position(this.char, rrow, rcol, dir, this.depthLife - 1));
			}
		}

		decide(goal) {
			// console.log("goal dir: "+goal)
			let choice = this.newPos(goal);
			let forward = this.forward();

			if (choice.distance < forward.distance && choice.distance < MAXDISTANCE()){
				return choice;
			}
			else if (forward.distance < MAXDISTANCE()){
				return forward; 
			}
			else
				return undefined;
		}

		consistentDecide() {
			let forward = this.forward();
			if (forward.distance < this.distance)
				return forward; //consistent
			else
				return this.euclideanDecide(); // try something else

		}

		euclideanDecide() {
			let directions = [this.left(), this.right(), this.forward()];
			directions.sort(function(a, b){
				return b.distance - a.distance; // backwards to use pop
			});
			let choice = directions.pop();
			if (choice.distance < MAXDISTANCE())
				return choice;
			else
				return undefined;
		}


		smaller(pos) {
			return this.distance < pos.distance;
		}
			
	}

function traverse(char, destinations) {
	let end = [false];
	let started = [false];
	char.traverseLoop = true;
	function t() {
		if (!char.traverseLoop){char.destinations = []; return;}
		if (char.destinations.length === 0 && !started[0] && char.done){
			started[0] = true;
			char.destinations = destinations.slice(0);
			if (!end[0])
				char.destinations.reverse();
			end[0] = !end[0];
			function delayWalkPath(char){
				function delay() {
				}
				return delay;
			}
			// walkPath(char);
			WalkPathRecursiveCallback(char, char.path);
			//setTimeout(delayWalkPath(char), 0); // if no destinations
			//return;
		}

		if (char.destinations.length === 0 && started[0])
			started[0] = false;

		setTimeout(t, char.speed * 2) // checks on this interval, usually ignored
	}
	t();
}

function delayedWalkPath(char, time){
	function delay() {
		walkPath(char);
	}
	setTimeout(delay, time)
}

function walkPath(char, callback) {
	let pathId = uid();
	char.walkPathId = pathId;
	let selectedCoordinates = [char.selectedRow, char.selectedCol];
	let callback2 = char.callbackList.shift();
	function w(){
		if (pathId !== char.walkPathId) return;
		
		let pos = char.path.pop(); //pop new position
		if (pos === undefined){
			//console.log(char.character+": no more steps. loading new path");

			char.done = true;
			let dest = char.destinations.pop() // we don't really need destinations yet. 1 path array is enough
			if (dest === undefined){
				//console.log(char.character+": no more destinations.");
				//char.update2(char.row, char.col) // stand animation
				if (callback) callback();
				if (callback2) callback2();
				return // done
			}
			if (char.debug)
				console.log(char.character + ": find script "+dest[0] + " " + dest[1])
			char.go(dest[0], dest[1])
			return;
		}


		if (pathId !== char.walkPathId) return;
		
		char.done = false;

		if(!char.updating){
			char.updating = true;
			char.update2(pos.row, pos.col); // causes still animation when blocked by obstacle // update takes time			
		}
		else {
			char.go(pos.row, pos.col);
			return;
		}


		if (!isUser(char) && (char.interrupted)) // civilian panic will interrupt
			char.path.push(pos); 

		setTimeout(w, char.speed);
	}
	w();
}

function walkPathModular(char, path, callback) {
	let pathId = uid();
	char.walkPathId = pathId;
	let selectedCoordinates = [char.selectedRow, char.selectedCol];
	let callback2 = char.callbackList.shift();
	let fetchMode = [false]
	
	function w(){
		if (pathId !== char.walkPathId) return;
		
		//if (char.updating) setTimeout(w, char.speed / 10);

		let pos = path.pop(); //pop new position
		if (pos === undefined){
			//console.log(char.character+": no more steps. loading new path");

			char.done = true;
			let dest = char.destinations.pop() // we don't really need destinations yet. 1 path array is enough
			if (dest === undefined){
				//console.log(char.character+": no more destinations.");
				if (callback) {callback();/*if(char.character === "bunny")console.log('*squeek*')*/}
				if (callback2) {callback2();/*if(char.character === "bunny")console.log('*squeek 2*')*/}
				//char.update2(char.row, char.col) // stand animation
				//faceStill(char, char.facing); // allows for moving while not moving - sliding
				return // done
			}
			if (char.debug)
				console.log(char.character + ": find script "+dest[0] + " " + dest[1])
			//if(char.character === "bunny")console.log('*squeek going to destination*');
			char.go(dest[0], dest[1], callback)
			return;
		}


		if (pathId !== char.walkPathId) return;
		
		char.done = false;
		if(!char.updating){ // this strategy - w should be a callback to update
			char.updating = true;
			// if (char.character === 'ju' && char.idx === 2) {
			// 	console.log('updating to: ' + pos.row + " - " +pos.col)
			// 	window.map.floorTileImg = "images/map/blueroomfloor.png"
			// 		let tile = window.map.layTile(pos.row, pos.col,1.5);
			// 		tile.style['z-index'] = 1000;
			// 		tile.style['opacity'] = .3;
			// 		tile.lastChild.style.borderRadius = "50px"
			// 	window.map.floorTileImg = "images/map/roomfloor.png"
			// }
			char.update2(pos.row, pos.col); 	
		}
		else { // this else statement 
			function go(){char.go(char.selectedRow, char.selectedCol, callback2);} // this reset my fetch mode
			if (char === window.user) // unsure
				setTimeout(go, char.speed/10); // works for user
			else
				if (char.fetchMode && !fetchMode[0]) {
					fetchMode[0] = true;
					setTimeout(go, char.speed); // works for bunnies but not ju
				} else if(!char.fetchMode) setTimeout(go, char.speed);
			return;
		}


		if (!isUser(char) && (char.interrupted)) // civilian panic will interrupt
			path.push(pos); 

		setTimeout(w, char.speed);
	}
	w();
}

function WalkPathRecursiveCallback(char, path, callback) {
	let sRow = [char.selectedRow];
	let sCol = [char.selectedCol];
	let walkId = uid();
	char.walkId = walkId;
	
	function w(){
		if (char.walkId !== walkId)return;

		if (char.callback) callback = char.callback;
		if(sRow[0] !== char.selectedRow || sCol[0] !== char.selectedCol){
			let newpath = greaterWallSearch(char.row, char.col, char.selectedRow, char.selectedCol);
			if (newpath.length > 0){
				sRow[0] = char.selectedRow;
				sCol[0] = char.selectedCol;
				path = newpath;
			} else {
				char.done = true;
				char.go(sRow[0], sCol[0])
				return;
			}
		}
	
		
		//if (char.updating) setTimeout(w, char.speed / 10);
		//if (char.character === 'alice') console.log("pop step")
		let pos = path.pop(); //pop new position
		if (pos === undefined){
			if(char.debugNavi) console.log("walkPathRecursive: no more steps, run callback.");


		
			let dest = char.destinations.pop() // we don't really need destinations yet. 1 path array is enough
			if (dest === undefined){
				if (callback) {callback();/*if(char.character === "bunny")console.log('*squeek*')*/}
				else if (char.callback) {
					let c = char.callback
					char.callback = null;
					c();
				}
				//char.update2(char.row, char.col) // stand animation
				// function still(){
				// 	faceStill(char, char.facing); // allows for moving while not moving - sliding
				// 	char.done = true;
				// }
				// if (!char.done)
				// 	setTimeout(still, char.speed)
				
				return // done
			}
			if (char.debug)
				console.log(char.character + ": find script "+dest[0] + " " + dest[1])
			//if(char.character === "bunny")console.log('*squeek going to destination*');
			char.go(dest[0], dest[1], callback) // it would be done due to updateC
			return;
		}
		
		//char.done = false;
		//if (char.character === 'alice') console.log("walking step")
		let interrupted = (window.user.row === pos.row && window.user.col === pos.col)
		if (!isUser(char) && (interrupted)){
			path.push(pos);
			//setTimeout(w, char.speed);
			//if(char.character === 'bunny')console.log("*eek* pushed position")
		}
		char.updateC(pos.row, pos.col, null, null, w);


		//setTimeout(w, char.speed);
	}
	w();
}

function singleSearch(row, col, selectedRow, selectedCol) { // cheap recallable euclidean search
	let facing = euclideanFace(row, col, selectedRow, selectedCol);
	let position = new Position({selectedRow: selectedRow, selectedCol: selectedCol}, row, col, facing, MAXDEPTH());
	if (row === selectedRow && col === selectedCol)
		return [position];
	let directions = [position.left(), position.right(), position.forward()];
		directions.sort(function(a, b){
			return a.distance - b.distance; 
		});
	let choice = directions.shift();
	while(choice && choice.distance === MAXDISTANCE())
		choice = directions.shift();
	if (choice)	return [choice]
	else return []


}

function wallSearch(row, col, selectedRow, selectedCol) {

	let path = [];
	let facing = euclideanFace(row, col, selectedRow, selectedCol); 

	let position = new Position({selectedRow: selectedRow, selectedCol: selectedCol}, row, col, facing, MAXDEPTH());
	if (row === selectedRow && col === selectedCol)
		return [position];
	let directions = [position.left(), position.right(), position.forward()];
		directions.sort(function(a, b){
			return a.distance - b.distance; 
		});

	let oldPosition = position;
	position = directions.shift(); // changed to shift not pop
	while(position && position.distance < MAXDISTANCE()) {
		r(position, path);
		if (path.length > 0){
			return path;
		} // if path is 0, try again on other 2 directions
		position = directions.pop();
	}
	return [];
}

class Safepoint {
	constructor(idx) {
		this.idx = idx;
		this.safepoints = window.map.safepoints;
		this.point = this.safepoints[idx];
		if (this.point)
			this.row = this.point[0];
			this.col = this.point[1];

	}

	next() {return new Safepoint(this.idx + 1);}
	prev() {return new Safepoint(this.idx - 1);}
}

function nearestLiberty2D(row, col, selectedRow, selectedCol){
	let liberties = [];
		let offsets = [  [-1,0],
					 [0,-1], [0,1],
						 [1,0]]
		for (let i = 0; i < offsets.length; i++) {
			let lib = offsets.shift();
			liberties.push([selectedRow + lib[0], selectedCol + lib[1]]);
		}

		for (let i = 0; i < liberties.length; i++) {
			if (!check(map,liberties[i][0], liberties[i][1]))
				liberties.splice(i, 1);
			
		}

		if (liberties.length === 0)
			return null

		let closestIdx = 0;
		let closestEntry = liberties[0];
		let smallestDist = distance(row, closestEntry[0], col, closestEntry[1]);
		for(const [i, val] of liberties.entries()){
			let dist = distance(row, val[0], col, val[1]);
			if (dist < smallestDist) {
				closestIdx = i;
				closestEntry = liberties[closestIdx];
				smallestDist = dist;
			}
		}
		return {row: closestEntry[0], col:closestEntry[1]}
}

function nearestLiberty(row, col, selectedRow, selectedCol){
	let liberties = [];
		let offsets = [[-1,-1], [-1,0], [-1, 1],
						[0, -1], [0,1],
						[1,-1], [1,0],[1,1]]
		for (let i = 0; i < offsets.length; i++) {
			let lib = offsets.shift();
			liberties.push([selectedRow + lib[0], selectedCol + lib[1]]);
		}

		for (let i = 0; i < liberties.length; i++) {
			if (!check(map,liberties[i][0], liberties[i][1]))
				liberties.splice(i, 1);
			
		}

		if (liberties.length === 0)
			return null

		let closestIdx = 0;
		let closestEntry = liberties[0];
		let smallestDist = distance(row, closestEntry[0], col, closestEntry[1]);
		for(const [i, val] of liberties.entries()){
			let dist = distance(row, val[0], col, val[1]);
			if (dist < smallestDist) {
				closestIdx = i;
				closestEntry = liberties[closestIdx];
				smallestDist = dist;
			}
		}
		return {row: closestEntry[0], col:closestEntry[1]}

}

function nearestSafepointRelative(row, col, destRow, destCol) {
	let nearestPoints = []
	for (let idx = 0; idx < map.safepoints.length; idx++){
		let sp = new Safepoint(idx);
		let path = wallSearch(row, col, sp.row, sp.col);
		sp.path = [];
		if (path.length > 0){
			sp.distance1 = path.length;
			let path2 = wallSearch(sp.row, sp.col, destRow, destCol);
			if (path2.length > 0) 
				sp.distance2 = path2.length
			else
				sp.distance2 = distance(sp.row, destRow, sp.col, destCol) * 3; // exclude, if wall search could reach something else
			sp.distance = sp.distance1 + sp.distance2;
			sp.path = path;
		}
		else
			sp.distance = MAXDISTANCE();
		nearestPoints.push(sp);
	}
	nearestPoints.sort(function(a, b){return a.distance - b.distance;});
	let nearest = nearestPoints[0];
	if (nearest)
		return {path:nearest.path, safepoint: nearest}
	else
		return {path:[], safepoint:undefined}; // should never happen by definition of safe point


}

function nearestSafepoint(row, col) {
	let safepoints = [];
	for (let idx = 0; idx < map.safepoints.length; idx++){
		let sp = new Safepoint(idx);
		sp.distance = distance(row, sp.row, col, sp.col);
		safepoints.push(sp);
	}

	safepoints.sort(function(a, b){
		return a.distance - b.distance; 
	});

	for (let safepoint of safepoints) { // ensure closest has a path before returning the path
		let path = wallSearch(row, col, safepoint.row, safepoint.col);
		if (path.length > 0)
			return {path:path, safepoint:safepoint};
	}
	return {path:[], safepoint:undefined}; // should never happen by definition of safe point
}


function pushPath(finalPath, path){
	for (let i = 0; i < path.length; i++)
			finalPath.push(path[i]);
}

function safeSearch(row, col, selectedRow, selectedCol){
	// identify startSafepoint = nearestSafepoint(row, col)
	// push destination = new Position({selectedRow: safepoint.row, selectedCol: safepoint.col}, row, col, facing, maxDepth)
	// push destSafepoint = nearestSafepoint(selectedRow, selectedCol)
	// push set of points from destSafepoint to startSafepoint
	if (!check(window.map, selectedRow, selectedCol))
		return [];
	let safepoints = window.map.safepoints;
	let finalPath = []
	let facing = euclideanFace(row, col, selectedRow, selectedCol); 
	let position = new Position({selectedRow: selectedRow, selectedCol: selectedCol}, selectedRow, selectedCol, facing, MAXDEPTH());
	finalPath.push(position)
	let startToSafepoint = nearestSafepoint(row, col); // destination is first thing on the stack
	let endFromSafepoint = nearestSafepoint(selectedRow, selectedCol);
	endFromSafepoint.path.reverse(); // destination is now first thing on the stack
	pushPath(finalPath, endFromSafepoint.path);
	// pushPath(finalPath, startToSafepoint.path);
	// return finalPath;

	if (!startToSafepoint.safepoint || !endFromSafepoint.safepoint)
		return [];
	////////// the middle /////////
	let startIdx = startToSafepoint.safepoint.idx;
	let endIdx = endFromSafepoint.safepoint.idx; // unsafe code
	// there might not be a path, if script is under the wall

	let dir = 0;
	let dirStr = "none";
	if (startIdx < endIdx) {dir = 1; dirStr = "prev";}
	else if (startIdx > endIdx) {dir = -1; dirStr = "next";} 
	else { /// safepoint distance == 0
		pushPath(finalPath, startToSafepoint.path);
		return finalPath;
	}

	/// safepoint distance > than 0

	let count = (endIdx - startIdx) * dir;
	let safepoint = new Safepoint(endIdx);
	while(count){
		let next = safepoint[dirStr]();

		let path = wallSearch(next.row, next.col, safepoint.row, safepoint.col); // reversed
		pushPath(finalPath, path);

		safepoint = next;
		count--;
	}
	pushPath(finalPath, startToSafepoint.path);
	return finalPath;
}

function rd(pos, goal, path) { // recursion triggered after wall is hit
	// if (pos.char.character === "lockune")
	// 	console.log(pos.char.character + " going: "+pos.dir + " - rd")
	if (pos.isWall){
		//console.log("rd: entered a wall")
		return // entered a bad place
	}

	if (path.length > 0){
		path.push(pos);
		return;
	}

	if (pos.found){
		// console.log("rd: " + pos.dir + " - found destination")
		path.push(pos);
		return;
	}

	if (pos.depthLife === 0){
		// console.log("rd: " + pos.dir + " - depth limit reached")
		return;
	}

	let forward = pos.forward();
	let choice = pos.decide(goal);
	if (choice === undefined){
		return;
	}

	// console.log("forward - "+forward.dir + " - "+forward.distance)
	// console.log("choice - "+choice.dir + " - "+choice.distance)
	if (forward.dir === choice.dir){
		rd(forward, goal, path, true); // this true goes no where
		if (path.length > 0)
			path.push(pos); 
	}
	else{
		// console.log("return to outer loop")
		r(choice, path);
		if (path.length > 0)
			path.push(pos); 
		// later: only push when found solution
		//r(choice, path)
		// check pathlength and push after this
	}

	// if choice is forward, recur
	// if it's not forward, follow and change  goal to back
	
}

function r(pos, path, consistentSearch){
	// TODO: use consistent search on both sides before hitting a wall
	// in the search method

	// if (pos.char.character === "lockune")
	// 	console.log(pos.char.character + " going: "+pos.dir + " - r")

	if (pos.isWall){
		// console.log("r: entered a wall")
		return // entered a bad place
	}

	if (path.length > 0){
		path.push(pos);
		return;
	}

	if (pos.found){
		// console.log("r: " + pos.dir + " - found destination")
		path.push(pos);
		return;
	}

	if (pos.depthLife === 0){
		// console.log("r: " + pos.dir + " - depth limit reached")
		return;
	}

	if (pos.hitWall) {
		let pathLeft = [];
		let pathRight = [];
		let left = pos.left();
		let right = pos.right();
		//console.log("start left path");
		rd(left, pos.dir, pathLeft); // will take forever to return, as it morphs
		//console.log("left path:");
		//console.log(pathLeft);
		//console.log("#########");
		//console.log("start right path");
		rd(right, pos.dir, pathRight); // but that's why we have depth limit
		//console.log("right path:");
		//console.log(pathRight);

		let shortestPath = pathLeft;
		if (shortestPath.length === 0 || (pathRight.length != 0 
		 								  && pathRight.length < shortestPath.length))
			shortestPath = pathRight;

		// console.log("shortest path:");
		// console.log(shortestPath);

		for (let i = 0; i < shortestPath.length; i++)
			path.push(shortestPath[i]);

		// path = shortestPath; // did this not work
		if (path.length > 0)
			path.push(pos);

		return;
	}


	if (consistentSearch)
		r(pos.consistentDecide(), path, true);
	else
		r(pos.euclideanDecide(), path); 

	if (path.length > 0)
		path.push(pos);

}