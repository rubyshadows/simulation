class HeartScript extends NPCScript {
	constructor(script, closeText, successText, arrivedText){
		super(script);
		this.closeText = closeText;
		this.successText = successText; //output
		this.arrivedText = arrivedText;
		this.messages = ["Testing 1", "Testing 2", "Testing 3"];
	}

	activatedCondition(npc, data) { 
		heartCommand(npc, this, data);
	}

	arrivedFunction(npc, callback){
		//npc.faceAuto("Down");
		npc.arrived = true;
		npc.scene(false);
		if (callback) callback(); //for top liberty search
	}

	upload(char) {
		char.heartScript = this; // script
		char.closeText = this.closeText;
		char.successText = this.successText;
	}
}

function parseLines(result, debug){
	let commandLines = [];

	function pushCommand(line){
		let inputs = line.split(' ');
		let args = [];
		let command = inputs.shift().replace("\n","");
		if (!command) return;
		for (let input of inputs) {
			args.push(inputs.shift().replace("\n",""));
		}

		commandLines.push({command: command, args: args})
	}

	if(testForOutput(result)) {
		let lines = result['output'].split(/[\n;]+/);
		if (debug) console.log("parse lines: "+lines)
		for (let i = 0; i < lines.length; i++) {
			if (debug) console.log("parse line: "+lines[i])
			let line = lines[i]
			pushCommand(line)
		}

	}
	return commandLines
}

function parseInput(){
	let commandLines = parseLines({output:window.datastore.scripttext});
	commandLines.scriptname = window.datastore.scriptname;
	return commandLines
}

function heartCommand(npc, script, data) {

	if(testForOutput(data)){
		//if (npc.character === "brambot") console.log("brambot: script detected")
		if (testForCollect(script, data) || testForEdit(script, data)) {
			npc.responsive = true;
			//if (npc.character === "brambot") console.log("brambot: heartscript collected")
			npc.changeForm();
			obey(npc, data);
			if (!npc.isGhost()){
				npc.flavorText = script.successText;
				npc.open(npc.airtime);
			}
		}
		else {
			//if (npc.character === "brambot") console.log("brambot: heart not collected")
			if (npc.befriend) {
				//if (npc.character === "brambot") console.log("brambot: befriended")
				obey(npc, data);
			} else {/*if (npc.character === "brambot") console.log("brambot: not befriended")*/}
		}
		
	}
}


function obey(npc, result) {
	let commandLines = [];
	//if (npc.character === 'alice') console.log("alice: obey detected: "+result.output)
	//if (npc.character === "brambot") console.log("brambot: obey detected: "+result.output)
	if(testForOutput(result)) {
		let lines = result['output'].split(/[\n;]+/);
		for (let i = 0; i < lines.length - 1; i++) {
			let line = lines[i].toLowerCase();
			let inputs = line.split(' ');
			let args = [];
			let exec = inputs.shift();
			if (npc.character === 'alice') console.log("exec: "+exec)
			if (exec) exec = exec.replace("\n",""); else continue;
			if (exec !== npc.character && exec !== npc.species && exec !== npc.subspecies) continue;
			let command = inputs.shift();

			if (npc.character === 'alice') console.log("command: "+command)
			if (command) command = command.replace("\n","");

			if (!command) continue;
			for (let input of inputs) 
				args.push(inputs.shift().replace("\n",""));

			if (npc[command])
				npc[command](args);
			
		}
	}
}


function testForDrop(result){return result['method'] === 'drop';}

function testForRun(result) {return result['method'] === 'run';}
function testForTest(result) {return result['method'] === 'test';}
function testForPlace(result) {return result['method'] === 'place';}

function testForCollect(script, result){return result['method'] === 'collect' && result['script']['id'] === script.id;}
function testForEdit(script, result) {return result['method'] === 'edit' && result['script']['id'] === script.id;}

function testForSelected(script) {return window.selectedID === script.id;}
function testForRunSelected(script, result) {return result['method'] === 'run' && testForSelected(script);}
function testForTestSelected(script, result) {return result['method'] === 'test' && testForSelected(script);}
function testForPlaceSelected(script, result) {return result['method'] === 'place' && testForSelected(script);}

function testForOutput(result) {return result.hasOwnProperty('output');}
function testForScript(result) {return result.hasOwnProperty('script');}
function testForHeart(result) {
	if (testForScript(result))
		for(let char of window.characterArray)
			if (char !== window.user && char.heartScript.id === result['script'].id)
				return true;
	return false;
}

function testForHeal(user) {return user['method'] === 'heal';}
function testForTouch(user) {return user['method'] === 'touch';}
function testForNew(user) {return user['method'] === 'new';}

function testForHealthy() {return !window.user.isGhost();}
// will probably need to update to runWithID to fix multiclicker exploit

function makeHeartScript(npcScript, flavorText, color){
	let closeText = flavorText; // the way process works now
	let successText = ""; // for output
	let heart = new HeartScript(npcScript, closeText, successText);
	heart.appendColorTile(color);
	return heart;
}

class NavigationHeart extends HeartScript {
	constructor(script, arrivedText) {
		super(script, '', '', arrivedText);
	}

	arrivedFunction(npc) {super.arrivedFunction(npc); npc.flavorText = this.arrivedText; npc.forceOpen();}
}

class AliceHeart extends HeartScript {
	constructor(script) {
		let closeText = "And so the story unfolds."; // the way process works now
		let successText = "I hear ya."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}

	activatedCondition(npc, data) { 
		//////////// Test For Heart Script ////////////
		heartCommand(npc, this, data);
		npc.responsive = false; // alice's heartfile sets her responsive to false, subverting standby
		let homeScript = nearestAuthorScript("Alice", 127, 14);
		let cartridgeScript = nearestAuthorScript("Lockune", 134, 34);
		let neutralZone = {row: homeScript.row - 5, col: homeScript.col};
		let message = [""];
		let seconds = [6000];

		// function faceAuto(){
		// 	npc.faceAuto("Down"); 
		// 	if(npc.callback) {
		// 		npc.callback();
		// 		npc.callback = null;
		// 	}
		// }
		function faceAuto(){npc.faceAuto("Down");
							npc.flavorText="Collect this"; 
							npc.open(4000);}
		
		function open() {
			npc.flavorText = message[0];
			npc.openSilence(seconds[0]);
		}
		function trueBaseSpeed(){npc.baseSpeed = npc.trueBaseSpeed;}
		function critterSpeed(){npc.baseSpeed = CRITTERSPEED();}
		function walk(){npc.walk();}
		function run(){npc.run();}
		function come() {npc.come(walk); npc.run();}
		function aliceReturn(callback){npc.return(); critterSpeed();}
		function home(){
			npc.fetch(homeScript.row-1,homeScript.col, faceAuto);
			npc.run();
		}
		function cartridge(callback){
			npc.followRange = 9; 
			npc.fetch(cartridgeScript.row, cartridgeScript.col, callback); 
			npc.run();
			critterSpeed();
		}
		function neutral(callback, run){
			if (callback)
				npc.callback = callback;
			else npc.callback = null;
			npc.standby(); npc.critterspeed();
			if (run){npc.truespeed(); npc.run();}
			npc.go(neutralZone.row, neutralZone.col, faceAuto);
		}
		function makePrint(m){function f(){/*console.log("alice: "+m)*/} return f;}


		//////////// Test For Heal ////////////
		//if (testForHeal(data)) npc.baseSpeed = npc.trueBaseSpeed;

		//////////// Test For Collected Support Script ////////////
		if (testForCollect(homeScript, data)) {
			let output = data['output'].toLowerCase().replace('\n', '');

			if (output !== 'cannonball removed' && output !== 'no cannonball found') return
			// walk to neutral
			
			//neutral();
			come();
		}
		
		//////////// Test For Collected Bram Heart ////////////
		if (testForCollect(getBram().heartScript, data)){
			let output = parseLines(data);

			for (let lines of output){
				let command = lines.command;
				//console.log(lines) // test this now
				let args = lines.args;
				let print = makePrint(command.toLowerCase() + " " + args[0].toLowerCase())
				switch(command.toLowerCase() + " " + args[0].toLowerCase()) {
					case "batteries remaining:":print(); message[0] = args[1]+" more left";seconds[0]=6000; open(); break; // come, 2 to go
					case "brambot: loading":print(); message[0] = "Collect this"; seconds[0]=10000; home(); trueBaseSpeed(); break; // remove canon ball - home
					case "brambot: launching":print(); come(); critterSpeed(); break; // silent, come, critterspeed
					case "brambot defeated":print(); aliceReturn(); message[0] = "Too easy"; seconds[0]=6000; open(); break; // Too easy // return()
					case "brambot reengage":print(); come(); break; // run to 10 rows above brams heart - home
					case "brambot: no":print(); cartridge(come); break; // silent, run to cartridge critterspeed
				}
			}

		}

	}
}

class JuHeart extends HeartScript {
	constructor(script) {
		let closeText = "Welcome adventurer. Click to move, and use the mousewheel or a thumb swipe to scroll the screen. Please follow me."; // the way process works now
		let successText = "So far so good."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
		this.messages = [''];
	}

	activatedCondition(npc, data){} // will lock until tutorial finishes

}

class SuzieHeart extends HeartScript {
	constructor(script) {
		let closeText = "Having a nice day?."; // the way process works now
		let successText = "Got it!"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class GokuHeart extends HeartScript {
	constructor(script) {
		let closeText = "Bowwow!"; // the way process works now
		let successText = "*pants*"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class LockuneHeart extends HeartScript {
	constructor(script){
		let closeText = "Welcome adventurer. Click to move, and use the mousewheel or a thumb swipe to scroll the screen. Please follow me."; // the way process works now
		let successText = "So far so good."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
		this.messages = [''];
	}

}

class GuillaumeHeart extends HeartScript {
	constructor(script) {
		let closeText = "You must remember to <span style='color: orange'>Test</span>."; // the way process works now
		let successText = "Heh heh."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class AaronHeart extends HeartScript {
	constructor(script) {
		let closeText = "I know you'll do great."; // the way process works now
		let successText = "Indeed."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}
class KrisHeart extends HeartScript {
	constructor(script) {
		let closeText = "How's it going?"; // the way process works now
		let successText = "Oh yeah!"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class PoppyHeart extends HeartScript {
	constructor(script) {
		let closeText = "*pants*"; // the way process works now
		let successText = "Woof!"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class NaomiHeart extends HeartScript {
	constructor(script) {
		let closeText = "You should turn experience into power!"; // the way process works now
		let successText = "Thank you."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class ScarletHeart extends HeartScript {
	constructor(script) {
		let closeText = "Do you like green tea?"; // the way process works now
		let successText = "Hello there."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class HorusHeart extends HeartScript {
	constructor(script) {
		let closeText = "You must grow stronger."; // the way process works now
		let successText = "Life is short."; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class SheldonHeart extends HeartScript {
	constructor(script) {
		let closeText = "*bzzzt*"; // the way process works now
		let successText = "*beep boop*"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class SynHeart extends HeartScript {
	constructor(script) {
		let closeText = "I live for my dogs at home."; // the way process works now
		let successText = "Wuff!"; // for output
		super(script, closeText, successText);
		this.appendColorTile('orange');
	}
}

class BrambotHeart extends HeartScript {
	constructor(script) {
		let closeText = "Docker D-Docker Hearts!!!"; // the way process works now
		let successText = "Welcome to Docker Hearts!!!"; // for output
		super(script, closeText, successText);
		this.appendColorTile('crimson');
	}

	activatedCondition(npc, data) { 
		//////////// Test For Heart Script ////////////
		heartCommand(npc, this, data);
		// let bram = getBram();

		// if (testForCollect(bram.heartScript, data)){
		// 	let output = parseLines(data);
		// 	let heart = this;
		// 	function speedyCritters(){
		// 		// quickens critters to match his speed
		// 		let critters = window.characterDict['critters']
		// 		// change critter speed with for loop - but need critter defaults and subspecies first
		// 	}
		// 	function 
		// 	for (let lines of output){
		// 		let command = lines.command;
		// 		console.log(lines) // test this now
		// 		let args = lines.args;
		// 		let print = makePrint(command.toLowerCase() + " " + args[0].toLowerCase())
		// 		switch(command.toLowerCase() + " " + args[0].toLowerCase()) {
		// 			//case "batteries remaining:":print(); message[0] = args[1]+" more left";seconds[0]=6000; open(); break; // come, 2 to go
		// 			//case "brambot: loading":print(); message[0] = "Collect this"; seconds[0]=10000; home(); trueBaseSpeed(); break; // remove canon ball - home
		// 			//case "brambot: launching":print(); come(); critterSpeed(); break; // silent, come, critterspeed
		// 			//case "brambot defeated":print(); aliceReturn(); message[0] = "Too easy"; seconds[0]=6000; open(); break; // Too easy // return()
		// 			case "brambot reengage":print(); ; break; // run to 10 rows above brams heart - home
		// 			//case "brambot: no":print(); cartridge(come); break; // silent, run to cartridge critterspeed
		// 		}
		// 	}

		// }
	}

}