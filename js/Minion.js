class Minion extends Civilian {
	// minions chase nearest civilian // they don't collect scripts
	// they run malicious code in your container when you come near
	// heal button moved to netnavi popover
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, flavorText,
    			ghostText, heartScript, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, flavorText,
    			ghostText, heartScript, scriptList)

		this.enemy = "civilian";
		this.faction = "minion";
	}

	npcUpdate() {
		// run near heartScript when opposing faction comes close to it - super.npcUpdate()

		// run malicious code in your container when you come near
		// walk towards nearest civilian - third breakthrough - time pressure
	}
}