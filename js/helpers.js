function coordinatesToTuples(coordinates){
	let destinations = [];
		for (let dest of coordinates)
			destinations.push([dest.row, dest.col]);
	return destinations;
}

function isUser(char){
	return window.user === char;
}

function setValues(obj, left, top, width, height){
	obj.style.left = left + "px";
	obj.style.top = top + "px";
	obj.style.width = width + "px";
	obj.style.height = height + "px";
}

function check(map, row, col, userObstacle){
	let usr = window.user;
	return map.barrierList[row +'-'+col] === undefined && (row >= 0 && col >= 0);
}

function distance(x1, x2, y1, y2) {
	let x = (x1 - x2); 
	let y = (y1 - y2);
	let sum = x*x + y*y;
	//console.log("x1: "+x1 + " - x2: " + x2 + " - y1: "+y1+" - y2: "+y2);
	//console.log("sqrt: "+ Math.sqrt(sum));
	return Math.sqrt(sum); 
}

function stringReplace(target, search, replacement) {
	if (!target)return "";
	return target.split(search).join(replacement);
}

function userInject(target) {
	return stringReplace(target, "_username_", window.datastore['userData']['user'].username)
}

function uid() {
	return Math.floor((1 + Math.random()) * 0x10000);
}