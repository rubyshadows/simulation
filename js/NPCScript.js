class NPCScript {
	constructor(script) {
		this.script = script;
		this.id = script.id;
		this.author = script.user;
		this.user = script.user;
		this.row = script.row;
		this.col = script.col;
		this.filename = script.filename;
		this.filetext = script.filetext;
		this.filetype = script.filetype;
		this.material = script.material;
		this.collectedList = script.collected_list;
		this.location = script.location;
		this.scriptColor = "none";
	}

	reloadDistance(char){
		this.distance = distance(char.row, this.row, char.col, this.col);
		this.path = getPath(char.row, char.col, this.row, this.col);
		if (this.path.length === 0)
			this.distance = MAXDISTANCE();
	}

	getPath(char) {
		this.path = getPath(char.row, char.col, this.row, this.col);
		return this.path;
	}

	appendColorTile(color){
		this.removeColorTile();
		let imgDiv = document.getElementById(this.script.id);
		let tile = document.createElement("div");
		imgDiv.append(tile);
		tile.style.position = "absolute";
		tile.style.top = '0px';
		tile.style.left = '0px';
		tile.style.width = '100%';
		tile.style.height = '100%';
		tile.style.opacity = '0.3';
		tile.style.background = color;
		tile.setAttribute('id', this.script.id + 'ColorTile');
	}

	removeColorTile(){
		let imgDiv = document.getElementById(this.script.id);
		let tile = document.getElementById(this.script.id + "ColorTile");
		if (tile)
			imgDiv.removeChild(tile);
	}
}