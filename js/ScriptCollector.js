class ScriptCollector extends NPC {
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode,
    			heartScript, n, getFurthest, scriptList) {
		
		super(character, imgCellWidth, imgCellHeight,
	    	  heartScript.row, heartScript.col, basePose, facing, baseSpeed, mode, heartScript.closeText);

		heartScript.upload(this);

		this.idx = 0;
		if (scriptList){
			this.scriptList = scriptList;
		}
		else {
			this.scriptList = [];
			this.gatherScripts(n, getFurthest); // populate npcScriptList
		}
		this.scriptList.push(heartScript); // so they return to the heart
		this.alive = true;
		this.baseFlavorText = this.flavorText;

		this.debug = false;
		this.traverseLoop = false;
		this.callbackList = [];

	}

	currentScript() {
		return this.scriptList[this.idx]
	}

	die() { // game phase
		this.baseFlavorText = this.flavorText;
		this.flavorText = "..."
		changeForm("ghost");
	}

	live() { // game phase
		this.flavorText = this.baseFlavorText;
		this.changeForm();
	}

	gatherScripts(n, getFurthest) {
		let npcScriptList = [];
		for (let script of window.map.rawscripts){
			npcScriptList.push(new NPCScript(script));
		}
		return this.loadScripts(npcScriptList, n, getFurthest);
	}

	loadScripts(npcScripts, n, getFurthest) {
		// gets the n nearest
		for (let npcScript of npcScripts){
			npcScript.reloadDistance(this);
		}
		npcScripts.sort(function(a, b){
			return a.distance - b.distance
		})
		if (getFurthest)
			this.scriptList = this.furthestScripts(npcScripts, n);
		else
			this.scriptList = this.nearestScripts(npcScripts, n);

		return this.scriptList;
	}

	nearestScripts(npcScripts, n){
		let res = [];
		for (let i = 0; i < n && i < npcScripts.length; i++){
			if (npcScripts[i].distance < MAXDISTANCE())
				res.push(npcScripts[i]);
			else
				n++;
		}
		return (res);
	}


	furthestScripts(npcScripts, n){
		let res = [];
		for (let i = npcScripts.length - 1; i > -1 && (npcScripts.length - 1) - i < n; i--){
			if (npcScripts[i].distance < MAXDISTANCE())
				res.push(npcScripts[i]);
			else
				n++;
		}
		return (res);
	}

	getScript(npcScript) {
		// go to loaded script
		let script = npcScript;
		script.reloadDistance(this);
		// this.selectedRow = script.row;
		// this.selectedCol = script.col;
		// if (this.debugNavi){
		// 	console.log("getScript - find script "+script.row + " " + script.col)
		// 	console.log("getScript - done before go: "+this.done)
		// }
		let lib = nearestLiberty2D(this.row, this.col, script.row, script.col);
		if(this.isNavi)
			this.go(lib.row, lib.col)
		else
			this.go(script.row, script.col) // here // navi search overwritten when !done
		// console.log('getScript - selected row: '+this.selectedRow)
		// console.log('getScript - selected col: '+this.selectedCol)
		// this.path = script.path;
		// this.path.shift(); // so it doesn't step on script;
		// walkPath(this);
	}

	getTargetScript(){
		this.getScript(this.scriptList[this.idx]);
	}

	next(){
		// load next script
		// change flavor text
		this.idx++;
		if (this.idx === this.scriptList.length)
			this.idx = 0;
	}

	prev() {
		// load previous script
		this.idx--;
		if (this.idx === -1)
			this.idx = this.scriptList.length - 1;
	}

	traverse() {
		// traverse scriptList
		let orderedScripts = this.nearestScripts(this.scriptList, this.scriptList.length);
		let destinations = coordinatesToTuples(orderedScripts);
		traverse(this, destinations);
	}

	makeTraverse(callFirst) {
		let char = this;
		function t() {
			if(callFirst)callFirst();
			let orderedScripts = char.nearestScripts(char.scriptList, char.scriptList.length);
			let destinations = coordinatesToTuples(orderedScripts);
			traverse(char, destinations);
		}
		return t;
	}

	makeFace(dir) {
		let char = this;
		function f() {
			char.faceAuto(dir);
		}
		return f
	}

	downloadHeart(heartScript) {
		//change flavor text in NPC.js to match script
		heartScript.upload(char);
	}
}

function addScript(npcScript){
	allScripts()[npcScript.author].push(npcScript);
}

function nearestAuthorScript(author, row, col) {
	let npcScripts = allScripts()[author];
	for (let script of npcScripts)
		script.reloadDistance({row:row, col:col});
	npcScripts.sort(function(a, b){
		return a.distance - b.distance
	})

	return npcScripts[0];
}

function nearestScript(row, col) {
	let npcScriptList = [];
	for (let script of window.map.rawscripts){
		npcScriptList.push(new NPCScript(script));
	}
	for (let npcScript of npcScriptList){
		npcScript.reloadDistance({row: row, col:col});
	}
	npcScriptList.sort(function(a, b){
		return a.distance - b.distance
	});
	for (let i = 0; i < npcScriptList.length; i++)
			if (npcScriptList[i].distance < MAXDISTANCE())
				return npcScriptList[i];
	return undefined;
}

function nearestScripts(row, col, n){
	let npcScriptList = [];
	let res = []
	for (let script of window.map.rawscripts){
		npcScriptList.push(new NPCScript(script));
	}
	for (let npcScript of npcScriptList){
		npcScript.reloadDistance({row: row, col:col});
	}
	npcScriptList.sort(function(a, b){
		return a.distance - b.distance
	});
	for (let i = 0; i < n; i++){
		if (!npcScriptList[i]) return res;
		if (npcScriptList[i].distance < MAXDISTANCE())
			res.push(npcScriptList[i]);
	}

	return res;
}

function orderScripts(row, col, npcScriptList, getFurthest) {
	for (let npcScript of npcScriptList){
		npcScript.reloadDistance({row: row, col:col});
	}
	npcScriptList.sort(function(a, b){
		if (!getFurthest)
			return a.distance - b.distance
		else
			return b.distance - a.distance
	});
	return npcScriptList;
}

function removeScript(npcScript){
	allScripts()[npcScript.author].remove(npcScript); // need to test this
}

function allScripts(){
	// NPCScripts indexed by author name
	if (window.scriptsDict)
		return window.scriptsDict
	else
		window.scriptsDict = {};

	let scripts = map.rawscripts;
	for (let script of scripts){
		if (!window.scriptsDict[script.user.username])
			window.scriptsDict[script.user.username] = []
		window.scriptsDict[script.user.username].push(new NPCScript(script));	
	}
	return window.scriptsDict
}

function getUserScripts(username){return allScripts()[username];}

function initScriptCollectors() {
	// callback after scripts are loaded
	//let titanHeartScript = nearestScript(12, 7); // wont work till after map calls back
	//let titanHeart = new GokuHeart(titanHeartScript);
	//let titan = makeScriptCollector("goku", "scriptSearch", titanHeart, 9, false);
	//titan.traverse();	
	window.characterArray = [];
	window.characterArray.push(window.user);
	window.characterDict = {};
	window.characterDict['collector'] = [window.user];

	let juHeart = new LockuneHeart(nearestAuthorScript("Ju", 19, 4));
	let tutorialHeartScripts = lockuneTutorialScripts(juHeart.row, juHeart.col); // heart script won't be on the list
	let ju = makeNetNavi("ju", juHeart, tutorialHeartScripts); // for now he'll begin moving immediately.
	ju.species = 'collectors'
	//ju.debugNaviSearch = true;
	window.characterArray.push(ju);
	//ju.baseSpeed = 100
	ju.run();

	let poppyHeart = new PoppyHeart(nearestAuthorScript("Poppy", 50, 32)); // nearest script of this character
	let krisHeart = new KrisHeart(nearestAuthorScript("Kris", 46, 32));
	let aaronHeart = new AaronHeart(nearestAuthorScript("Aaron", 45, 4));
	let guillaumeHeart = new GuillaumeHeart(nearestAuthorScript("Guillaume", 19, 31));
	let scarletHeart = new ScarletHeart(nearestAuthorScript("Scarlet", 113, 20));
	let horusHeart = new HorusHeart(nearestAuthorScript("Horus", 134, 5));
	let synHeart = new SynHeart(nearestAuthorScript("Syn", 136, 25));
	let sheldonHeart = new SheldonHeart(nearestAuthorScript("Sheldon", 27, 32))
	let naomiHeart = new NaomiHeart(nearestAuthorScript("Naomi", 72, 29))

	let aliceHeart = new AliceHeart(nearestAuthorScript("Alice", 14, 18));
	let gokuHeart = new GokuHeart(nearestAuthorScript("Goku", 32, 28))
	let suzieHeart = new SuzieHeart(nearestAuthorScript("Suzie", 93, 2))

	let bramHeart = new BrambotHeart(nearestAuthorScript("Brambot", 133, 13));

	let guillaume = makeCivilian("guillaume", "wallSearch", guillaumeHeart, 30, false);
	guillaume.traverse(); window.characterArray.push(guillaume);
	let aaron = makeCivilian("aaron", "wallSearch", aaronHeart, 30, false);
	aaron.traverse(); window.characterArray.push(aaron);
	let kris = makeCivilian("kris", "wallSearch", krisHeart, 30, false);
	kris.traverse(); window.characterArray.push(kris);
	let poppy = makeCivilian("poppy", "wallSearch", poppyHeart, 30, false);
	poppy.trueBaseSpeed = CRITTERSPEED()*1.3; poppy.baseSpeed = poppy.trueBaseSpeed;
	poppy.traverse(); window.characterArray.push(poppy);
	let scarlet = makeCivilian("scarlet", "wallSearch", scarletHeart, 30, false);
	scarlet.traverse(); window.characterArray.push(scarlet);
	let horus = makeCivilian("horus", "wallSearch", horusHeart, 30, false);
	horus.traverse(); window.characterArray.push(horus);
	let syn = makeCivilian("syn", "wallSearch", synHeart, 20, false);
	syn.traverse(); window.characterArray.push(syn);
	let sheldon = makeCivilian("sheldon", "wallSearch", sheldonHeart, 20, false);
	sheldon.traverse(); window.characterArray.push(sheldon);
	let naomi = makeCivilian("naomi", "wallSearch", naomiHeart, 30, false);
	naomi.trueBaseSpeed = CIVILIANSPEED() / 2;
	naomi.baseSpeed = CIVILIANSPEED() / 2;
	window.characterArray.push(naomi); traverse(naomi, window.map.safepoints);

	let alice = makeAlice("alice", "wallSearch", aliceHeart, 20, false);
	
	//alice.traverse(); 
	window.characterArray.push(alice);

	let goku = makeCivilian("goku", "wallSearch", gokuHeart, 20, false);
	window.characterArray.push(goku);
	let suzie = makeCivilian("suzie", "wallSearch", suzieHeart, 20, false);
	window.characterArray.push(suzie);
	traverse(goku, window.map.safepoints);suzie.species = 'collectors'
	traverse(suzie, window.map.safepoints);	goku.mode = "Run";

	let bramGoodMessages = ["Welcome to Docker Hearts!!!", "Welcome to Docker Hearts!!!", "Welcome to Docker Hearts!!!", "Docker D-D-Docker Hearts!!!"];
	let bramBadMessages = ["Destroy Docker Hearts!!!", "Destroy Docker Hearts!!!", "Docker D-D-Docker Hearts!!!", "Destroy Docker Hearts!!"];
	let brambot = makeBoss("brambot", "wallSearch", bramHeart, 0, false);
	window.characterArray.push(brambot);
	brambot.goodMessages = bramGoodMessages;
	brambot.badMessages = bramBadMessages;
	brambot.go(brambot.row, brambot.col + 1);
	brambot['standby']();
	brambot['silence']();

	let critters = [['brownkitten','*meow*'], ['brownpuppy','*wuff*'],
					['bunny','*squeek*'], ['graykitten','*meow*'], 
					['graypuppy','*wuff*']];
	let critterCount = 7;
	let critterScripts = nearestScripts(synHeart.row, synHeart.col, critterCount);
	let moreCritterScripts = nearestScripts(aliceHeart.row, aliceHeart.col, critterCount * 2);
	let sheepScripts = nearestScripts(naomiHeart.row, naomiHeart.col, 4);
	sheepScripts.shift();
	moreCritterScripts.shift();
	critterScripts.shift();
	critterCount--;

	for (let i = 0; i < critterCount; i++){
		let critterScript = critterScripts[i];
		critterScript = makeHeartScript(critterScript, critters[i % critters.length][1], '#eb93ff')
		let critterCharacter = critters[i % critters.length][0]
		let critter = makeCritter(critterCharacter, "wallSearch", critterScript, 25, false);
		//critter.come();
		window.characterArray.push(critter);
		critter.traverse();
		if (critterCharacter.includes("kitten")) {
			critter.subspecies = 'kittens';
			critter.truespecies ='kittens'; 
			critter.truesubspecies = 'kittens';
			critter.trueBaseSpeed = CRITTERSPEED()+(i * 50);
			critter.baseSpeed = critter.trueBaseSpeed
		} // powerful if block
		else if (critterCharacter.includes("puppy")) {
			critter.subspecies = 'puppies';
			critter.truespecies ='puppies'; 
			critter.truesubspecies = 'puppies';
			critter.trueBaseSpeed = CRITTERSPEED()+(i * 75);
			critter.baseSpeed = critter.trueBaseSpeed
		} // less subclasses
		else if (critterCharacter.includes("bunny")) {
			critter.subspecies = 'bunnies';
			critter.truespecies ='bunnies'; 
			critter.truesubspecies = 'bunnies';
			//critter.trueBaseSpeed = CRITTERSPEED()+(i * 100);
			//critter.baseSpeed = critter.trueBaseSpeed
		} 
		
	}

	for (let i = 0; i < moreCritterScripts.length; i++){
		let critterScript = moreCritterScripts[i];
		critterScript = makeHeartScript(critterScript, "*squeek*", '#eb93ff');
		let critterCharacter = "bunny";
		let critter = makeCritter(critterCharacter, "wallSearch", critterScript, 25, false); // occilate get furthest
		critter.trueBaseSpeed = CRITTERSPEED()+(i * 100);
		critter.baseSpeed = critter.trueBaseSpeed
		critter.traverse();
		critter.subspecies = 'bunnies'
		window.characterArray.push(critter);
	}
	//let debunny = window.characterArray[window.characterArray.length - 1]
	//debunny.debugFollow = true;
	//debunny.charDiv.style.background = 'black'

	let sheep = []
	for (let i = 0; i < sheepScripts.length; i++){
		let critterScript = sheepScripts[i];
		critterScript = makeHeartScript(critterScript, "*baa*", '#eb93ff');
		let critterCharacter = "sheep";
		let critter = makeCritter(critterCharacter, "wallSearch", critterScript, 25, false); // occilate get furthest
		//critter.follow(['naomi'])
		critter.subspecies = 'sheep'
		critter.trueBaseSpeed = (CIVILIANSPEED() / 2)+(i * 200);
		critter.baseSpeed = critter.trueBaseSpeed
		window.characterArray.push(critter);
		sheep.push(critter);
	}

 	/// populate character treehouse 
 	
 	for (let char of window.characterArray){
 		if (!window.characterDict[char.species]) window.characterDict[char.species] = [];
 		window.characterDict[char.species].push(char);
 		if (!window.characterDict[char.character]) window.characterDict[char.character] = [];
 		window.characterDict[char.character].push(char);
 	}
 	// gives us an array for each species
 	// and an array for each character - to hold duplicates

	for (let sheepu of sheep) sheepu.follow(['naomi'])
	alice.responsive = false;
 	alice.follow(['ju']);
	alice.followRange = 3;

	//for loop below shows how maps can be drawn using safe points
	let path = greaterWallSearch(aliceHeart.row, aliceHeart.col, bramHeart.row, bramHeart.col)
	//let path = wallSearch(aliceHeart.row, aliceHeart.col, bramHeart.row, bramHeart.col)
	// for (let p of path) {
	// 	window.map.floorTileImg = "images/map/portfloor.png"
	// 	let tile = window.map.layTile(p.row, p.col, 4);
	// 	tile.style['z-index'] = 0;
	// 	tile.style['opacity'] = 1;
	// }
	// 	window.map.floorTileImg = "images/map/roomfloor.png"

}

function lockuneTutorialScripts(row, col){
	let lockuneScriptList = [];
	let lockuneScripts = orderScripts(row, col, getUserScripts("Lockune"));
	let juScripts = []; // lay ju scripts first - use nearest Author script on each

	function testForBram(result) {
		let bramlines = [];
		if(testForOutput(result)) {
			let npc = window.characterDict['brambot'][0];
			let lines = result['output'].split(/[\n;]+/);
			//console.log(lines.length + " lines in output")
			for (let i = 0; i < lines.length - 1; i++) {
				let line = lines[i]
				let inputs = line.split(' ');
				let args = [];
				let exec = inputs.shift().replace("\n","");
				if (exec !== 'brambot') continue;
				let command = inputs.shift().replace("\n","");
				if (!command) continue;
				for (let input of inputs) {
					args.push(inputs.shift().replace("\n",""));
				}

				if (npc[command]){
					bramlines.push({command: command, args: args})
				}
				else{
					//console.log("found no "+npc.character+"["+command+"]")
				}
			}

		}
		return bramlines
	}

	function testForCollectLoud(npc, script, result){
		if (!(result['method'] === 'collect' && result['script']['id'] === script.id))
		{
			if (testForSelected(script)){
				//console.log('ju: selected, locking open')
				if (!window.connected)
					npc.flavorText = "Gotta connect <img src='images/icons/material.gif' class='tiny' /> to collect."
				else
					npc.flavorText = "Try <span style='color: green'>Collect</span>."
				if (!npc.locked)
					npc.open(6000);// maybe only if arrived
			}
			return false;
		}
		else
			return true;
	}

	function printNpc(npc, message, time) {
		npc.flavorText = message;
		npc.open(time);
	}

	let heartScripts = [ // 12 of these for a tutorial
	
		{ // talk.sh - echo hello 1 
			arrivedText: `To start the tutorial, click this script <img src='images/icons/bash.png' class='tiny'/> 
						  and then click <span style='color: green'>Collect</span>.`, // would be close text

			messages: [`You're wearing a <span style='color: orange'>Container Suit</span>.`,
						`It lets you execute code.`,
						`But you must connect <img src='images/icons/material.gif' class='tiny' />`,
						`Click <span style='color:#A6B1E1'>New Player</span> if you're new.`],

			activatedCondition: function(npc, result){
				//if(npc.debugNavi) console.log("in activated condition - done: "+npc.done)
				return testForCollectLoud(npc, this, result);
			}
			// needs logged in and logged off variants
		},
		{ // my_inventory.sh - ls 2
			arrivedText: "<span style='color: green'>Collect</span> this one.", // would be close text

			messages: [`These sheets of paper are <span style='color: orange'>Scripts</span>.`,
						`You get <span style='color:#A6B1E1'>Material</span> <img src="images/icons/material.gif" class='tiny'/>
						for collecting them.`,
						`Scripts execute when <span style='color: green'>Collected</span>.`,
						`Don't fear scripts.`],
			activatedCondition: function(npc, result){
				return testForCollectLoud(npc, this, result);
			}
		},
		{ // whereami.sh - pwd 3
			arrivedFunction: function(npc) {
				npc.arrived = true;
				npc.scene(false)
				npc.faceAuto("Down");
			},
			arrivedText: "Collect a <span style='color:orange'>Heart Script</span>.",
			messages: [`Notice the colored scripts on the ground
						<img src="images/icons/brownbash.png" class='tiny'/>
						<img src="images/icons/purplebash.png" class='tiny'/>`,
						`Those are <span style='color:orange'>Hearts Scripts</span>.`,
						`They heal fallen friends when collected.`,
						`Gold means <span style='color:orange'><strong>civilian heart</strong></span>`, 
						`While purple means <span style='color:purple'><strong>critter heart</strong></span>.`,
						`Always collect hearts first.`],
			activatedCondition: function(npc, result){
				if (testForHeart(result)){
					npc.walk();
					return true;
				} 
				return false;
			}
		},
		{ // make-lab.sh - mkdir dh-lab1↵ls 4
			arrivedFunction: function(npc) {
				let script = this;
				//npc.popId = uid();
				npc.arrived = true;
				npc.scene(false);
				function t(){
					npc.faceAuto('Left');
					npc.pathSearch = makeNaviSearch(npc);
				}
				npc.pathSearch = makeWallSearch(npc);
				npc.go(script.row, script.col+1, t);

			},
			arrivedText: "<span style='color:green'>Collect</span> this.", // would be close text

			messages: [`Your container has a <span style='color:#eb93ff'>Heart</span> script of its own.`,
						`You become a <span style='color:#A6B1E1'>Ghost</span> if you lose it.`,

						`A <span style='color:#A6B1E1'>Ghost</span> cannot <span style='color:green'>Drop</span>
						or <span style='color:green'>Collect</span> or any scripts.`,

						`The author steals material from you too.`,

						`For example...`
						],
			activatedCondition: function(npc, result){
				if(testForCollectLoud(npc, this, result)){
					npc.run();
					return true;
				}
				return false;
			}
		},
		{ // relocate.sh - mv * dh-lab1 5
			arrivedFunction: function(npc) {
				let script = this;
				//npc.popId = uid();
				npc.arrived = true;
				npc.scene(false);
				function t(){
					npc.faceAuto('Down');
					npc.pathSearch = makeNaviSearch(npc);
				}
				npc.pathSearch = makeWallSearch(npc);
				npc.go(script.row, script.col+1, t);

			},
			arrivedText: `Click your name and then click 
						  <span style='color:#eb93ff'>Heal</span> for 
						  <span style='color:#A6B1E1'>a New Heart Script</span>.`,
			messages: [`Don't worry, cadet.`],
			activatedCondition: function(npc, data){
				if (testForHeal(data)){
					npc.walk();
					return true;
				}
				return false;
			}
		},
		{ // healbell - click heart↵ls 6
			arrivedFunction: function(npc) {
				let script = this;
				npc.arrived = true;
				npc.scene(false);
				function t(){
					npc.faceAuto('Right');
					npc.pathSearch = makeNaviSearch(npc);
				}
				npc.pathSearch = makeWallSearch(npc);
				npc.go(script.row, script.col-1, t); // recursive - because navi search calls arrived function
			},
			arrivedText: `<span style='color:red'>Run</span> this script. Replace "XYZ" with a fruit.`,
			messages: [`Tweaking scripts is good.`,
					    `And then you can <span style='color:red'>Run</span> them.`,
						`But <span style='color:green'>Collect</span> executes the unchanged version.`
					    ],
			activatedCondition: function(npc, data){
				let ran = testForRunSelected(this, data);
				let healthy = testForHealthy();
				if (ran && !healthy){
					npc.flavorText = `<span style='color:#eb93ff'>Heal</span> first, and try again. 
									 Change <em>XYZ</em> to something else, and then click <span style='color:red'>Run</span>.`
					npc.open(6000);
				}
				if (ran && healthy) {
					let bram = window.characterDict['brambot'][0];
					let syn = window.characterDict['syn'][0];
					let alice = getAlice();
					function t(){alice.standby(); 
								 alice.silence(); traverse(alice, coordinatesToTuples(syn.scriptList));}		
					alice.fetch(bram.row-2, bram.col, t);
					alice.alicespeed();
					alice.run();
					npc.walk();
				}
				return ran && healthy;
			}
		},
		{ // copycat.sh - cp heart heartcopy↵ls 7 
			arrivedFunction: function(npc) {
				let script = this;
				npc.arrived = true;
				npc.scene(false);
				function t(){
					npc.faceAuto('Left');
					npc.pathSearch = makeNaviSearch(npc);
				}
				npc.pathSearch = makeWallSearch(npc);
				npc.go(script.row, script.col+1, t); // recursive - because navi search calls arrived function
			},
			arrivedText: "Please <span style='color:orange'>Test</span> this.",
			messages: [`You can <span style='color:orange'>Test</span> a script`,
						`<span style='color:orange'>Test Containers</span> get destroyed afterwards.`],
			activatedCondition: function(npc, data){
				let tested = testForTestSelected(this, data);
				let selected = testForSelected(this); // never works
				let collected = testForCollect(this, data);
				let healthy = testForHealthy();

				if (tested) {
					npc.run();
					return true;
				}

				if (selected && healthy) npc.flavorText = `<span style='color:orange'>Test</span> it as well.`
				if (selected && !healthy) npc.flavorText = `<span style='color:#eb93ff'>Heal</span> first, and then <span style='color:orange'>Test</span> it.`
				npc.open(6000);
				return false;
			}
		},
		{ // kats.sh - cat heart 8 // boss
			arrivedText: "<span style='color:#A6B1E1'>Copy</span> this.",
			messages: [`You can pick up scripts without executing them.`,
						`We call this <span style='color:#A6B1E1'>Copying</span> a script.`,
						`<span style='color:#A6B1E1'>Copy</span> doesn't execute anything.`,
						`It's useful for compiling many things together.`,
						`For example...`],
			activatedCondition: function(npc, data){
				let placed = testForPlaceSelected(this, data);
				let selected = testForSelected(this);

				if (placed) return true;
				if (selected){
					npc.flavorText = `<span style='color:#A6B1E1'>Copy</span> it.`
					npc.open(6000);
				}
				return false;
			}
		},
		{ // begone.sh - rm heartcopy 9
			arrivedText: "And <span style='color:#A6B1E1'>Copy</span> this.",
			messages: [`Good.`],
			activatedCondition: function(npc, data){
				let placed = testForPlaceSelected(this, data);
				let selected = testForSelected(this);

				if (placed) return true;
				if (selected){
					npc.flavorText = `<span style='color:#A6B1E1'>Copy</span> it.`
					npc.open(6000);
				}
				return false;
			}
		},
		{ // C - ... 10
			arrivedText: "And now <span style='color:green'>Collect</span> this.",
			messages: [`Great.`],
			activatedCondition: function(npc, data){
				let collected = testForCollectLoud(npc, this, data);
				let selected = testForSelected(this);
				if (collected) {
					npc.baseSpeed = npc.trueBaseSpeed;
					npc.walk(); 
					return true;
				}  

				if (selected){
					npc.flavorText = `<span style='color:green'>Collect</span> it.`
					npc.open(6000);
				}
				return false;
			}
		},
		{ // peekaboo - py kill 12 - boss file
			arrivedFunction: function(npc){
				npc.arrived = true;
				npc.scene(false)
				npc.faceAuto("Down");

				let bram = getBram();
				let alice = getAlice(); 
				let horus = window.characterDict['horus'][0]
				function t(){alice.walk(); alice.come();}
				function faceAuto(){alice.faceAuto("Down"); alice.flavorText="C'est brambot"; 
									alice.open(4000, t);}
				alice.fetch(bram.row-1, bram.col, faceAuto);
				alice.alicespeed();
				if (distance(alice.row, bram.row, alice.col, bram.col) > 10)
					alice.run();
			},
			arrivedText: `<span style='color:#A6B1E1'>Copy</span> or <span style='color:green'>Collect</span> this script 
						 and then collect <span style='color:orange'>Brambot's Heart Script</span>.`,
			messages: [`Now let's practice some combat.`,
					   `Your opponent is a robot named <span style='color:red'>Brambot</span>.`,
					   `We'll give Brambot a new cartridge, and the simulation will begin.`],
			activatedCondition: function(npc, data){
				let collected = testForCollect(this, data);
				let copied = testForPlaceSelected(this, data);
				let brambotheart = window.characterDict['brambot'][0].heartScript;
				let bramCollect = testForCollect(brambotheart, data)
				if (this.firstCondition)
					if (bramCollect) {npc.walk();return true;}
				if (collected || copied) {
					this.firstCondition = true;
					this.arrivedText = `Good. Now <span style='color:green'>Collect</span>
									 <span style='color:orange'>Brambot's Heart</span>
									 <img src="images/icons/brownbash.png" class='tiny'/>.`
					this.flavorText = this.arrivedText;
					npc.forceOpen(); // didn't overwrite - simply closed
				}
				return false;
			}
		},
		{ // ju file #1 - get all C files and run his heart for a ContextBonus? - gold material - versus brambot - output check
			arrivedFunction: function(npc){
				let script = npc.currentScript();
				function t(){
					npc.pathSearch = makeNaviSearch(npc);
					npc.faceAuto('Down'); 
					npc.arrived = true;
					npc.scene(false);
				}
				npc.pathSearch = makeWallSearch(npc); //needs top search
				npc.go(script.row-1, script.col, t);
			},
			arrivedText: "", // open 'defeat brambot.'
			messages: [`To defeat Brambot, you must recollect its <span style='color:orange'>Heart Script</span>
					   until it runs out of batteries.`,
					   `Now that Brambot is engaged, recollect its heart to start the simulation.`],
			activatedCondition: function(npc, data){
				let collected = testForCollectLoud(npc, this, data);
				let copied = testForPlaceSelected(this, data);
				let brambotheart = window.characterDict['brambot'][0].heartScript;
				let bramCollect = testForCollect(brambotheart, data);
				if (bramCollect){
					let bramCommands = testForBram(data);
					let correct = "defeated"
					if(bramCommands.length > 0){
						for (let i = 0; i < bramCommands.length; i++) {
							if (bramCommands[i].command === correct)
								return true;
						}
					}
				}
				return false;
			}
		},
		{ // ju file #2 - beaten brambot - teaches run - output check - maybe pipe to alice - "Ju is calling" - sleep but follow - like an alert
			arrivedFunction: function(npc){
				let script = npc.currentScript();
				function t(){
					npc.pathSearch = makeNaviSearch(npc);
					npc.faceAuto('Down'); 
					npc.arrived = true;
					npc.scene(false);
				}
				npc.pathSearch = makeWallSearch(npc); //needs top search
				npc.go(script.row-1, script.col, t);
			},
			arrivedText: "<span style='color:red'>Run</span> <strong>echo brambot come</strong>.",
			messages: [`Great. Now that you've beaten Brambot, you can control it.`,
						`Remember the <span style='color:#A6B1E1'>Script</span> button? 
						Click it and <span style='color:red'>Run</span> <strong>echo brambot come</strong>.`],
			activatedCondition: function(npc, data){
				let bramCommands = testForBram(data);
				let correct = "come"
				if(bramCommands.length > 0){
					for (let i = 0; i < bramCommands.length; i++) {
						if (bramCommands[i].command === correct)
							return true;
					}
				}
				return false;
			}
		},
		{ // ju file #3 - teaches drop - output check
			arrivedFunction: function(npc){
				let script = npc.currentScript();
				function t(){
					npc.pathSearch = makeNaviSearch(npc);
					npc.faceAuto('Down'); 
					npc.arrived = true;
					npc.scene(false);
				}
				npc.pathSearch = makeWallSearch(npc); //needs top search
				npc.go(script.row-1, script.col, t);
			},
			arrivedText: "<span style='color:green'>Drop</span> a script with <strong>echo brambot return</strong> as the code.",
			messages: [`Rather than typing it every time, you can automate the process by dropping the script.`],
			activatedCondition: function(npc, data){
				let bramCommands = testForBram(data);
				let correct = "return"
				if(bramCommands.length > 0){
					for (let i = 0; i < bramCommands.length; i++) {
						if (bramCommands[i].command === correct)
							return true;
					}
				}
				return false;
			}
		},
		{ // ju file #4 - teaches ContextFile - output check
			arrivedFunction: function(npc){
				let script = npc.currentScript();
				function t(){
					npc.pathSearch = makeNaviSearch(npc);
					npc.faceAuto('Down'); 
					npc.arrived = true;
					npc.scene(false);
				}
				npc.pathSearch = makeWallSearch(npc); //needs top search
				npc.go(script.row-1, script.col, t);
			},
			arrivedText: "<span style='color:red'>Run</span> <strong>ls brambot</strong>.",
			messages: [`Brambot leaves a key within your container when it's defeated, which will let you access level 2.`,
						`If you lose the key, or get a new container, you can't access level 2 until the key is replaced.`],
			activatedCondition: function(npc, data){
				if (testForRun(data)) {
					let commands = parseInput();
					//console.log('ju script #4 run')
					//console.log(commands)
					for (let command of commands){
						if (command.command === 'ls' 
							&& command.args[0] === 'brambot') return true
					}
				}
				return false;
			}
		},
		{ // ju file #5 - informs WorldMap, wrapup - output check
			arrivedFunction: function(npc){
				npc.faceAuto('Down'); 
				npc.arrived = true;
				npc.scene(false);
			},
			arrivedText: "",
			messages: [`For now, relax and enjoy some scripting.`],
			activatedCondition: function(npc, data){
				return false;
			}
		}
	];
	

	for(let i = 0; i < lockuneScripts.length; i++){
		let heart = heartScripts[i]
		if (!heart) break;
		let heartScript = new NavigationHeart(lockuneScripts[i], heart.arrivedText)
		if (heart.messages)
			heartScript.messages = heart.messages
		if (heart.activatedCondition)
			heartScript.activatedCondition = heart.activatedCondition
		if (heart.arrivedFunction)
			heartScript.arrivedFunction = heart.arrivedFunction

		lockuneScriptList.push(heartScript);

	}


	juScripts.push(nearestAuthorScript("Ju", 127, 20));
	juScripts.push(nearestAuthorScript("Ju", 118, 6));
	juScripts.push(nearestAuthorScript("Ju", 97, 6));
	juScripts.push(nearestAuthorScript("Ju", 90, 25));
	juScripts.push(nearestAuthorScript("Ju", 63, 21));
	let j = lockuneScriptList.length;
	for (let i = 0; i < juScripts.length; i++){
		let heart = heartScripts[j++];
		if (!heart) break;
		let heartScript = new NavigationHeart(juScripts[i], heart.arrivedText)
		if (heart.messages)
			heartScript.messages = heart.messages
		if (heart.activatedCondition)
			heartScript.activatedCondition = heart.activatedCondition

		lockuneScriptList.push(heartScript);
	}



	return lockuneScriptList;
}