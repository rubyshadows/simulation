class Civilian extends ScriptCollector {
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList)

		this.species = 'civilians'
		this.subspecies = 'civilians'
		this.truespecies = 'civilians'
		this.truesubspecies = 'civilians'

		this.enemies = [];
		this.heartColor = 'orange'
		this.heartScript.appendColorTile(this.heartColor);

		this.befriend = false;
		this.followMode = false;
		this.followRange = 1;
		this.trueFollowRange = this.followRange;
		this.arrivalRange = 25
		this.attackMode = false;
		this.silent = false;
		this.stopLoop = true;
		this.responsive = true;

	}

	precision(on){
		if (on) this.pathSearch = makeScriptSearch(this);
		else this.pathSearch = makeWallSearch(this);
	}

	befriended() {
		this.befriend = true;
	}

	come(callback) {
		this.follow(["collector"], callback);
	}

	defeated() {
		this.befriend = true;
		this.toGhost();
		this.return();
	}

	fetch(row, col, callback){
		this.standby();
		this.fetchMode = true;
		this.go(row, col, callback); ////////////////////////////////////////////////////////////
	}

	follow(target, callback) {
		if (target.length === 0) {return;}
		this.standby();
		//this.pathSearch = makeFollowSearch(this);
		this.followMode = true;
		let first = target.shift();
		let targets = window.characterDict[first].slice(0);
		let npc = this;
		function w() {npc.walk();}
		if (!callback) callback = w;
		this.followListC(targets, callback);
		//this.followListPlus(targets, callback);
	}

	followList(npcList, callback) {
		let char = this;
		let finishedCallback = [false]; // it's just one thread
		let followId = uid();
		char.followId = followId;
		if (Array.isArray(callback)) callback = callback.shift();
		function c (){
			if (char.followId !== followId) return;
			let npc = npcList[0];
			if (!npc) {
				char.return();
				return; // stops here - goes back to heart file in standby mode
			}
			if (!char.followMode) {return;}

			let currentDist = distance(char.row, npc.row, char.col, npc.col)
			let path;
			if (currentDist > 50) path = greaterWallSearch(char.row, char.col, npc.row, npc.col);
			else path = wallSearch(char.row, char.col, npc.row, npc.col);

			if (path.length > char.followRange) {
				for (let i = 0; i < char.followRange; i++) path.shift();
				let p = path.shift();
				let step = path[path.length-1];
				let stepDist = null;
				if (step) stepDist = distance(step.row, npc.row, step.col, npc.col)
				if (!step || currentDist > stepDist){
					//console.log(char.character + ": current distance is greater - " + currentDist + " > " + stepDist)
					char.go(p.row, p.col, c); // if choice doesn't bring u closer, don't interrupt
				}
			} else { // less than following range
				if (!finishedCallback[0]) {
					if (callback) callback();
					finishedCallback[0] = true;
				}
			}
			
			//if (path.length === 1) {recur anyway}
			setTimeout(c, char.speed);
		}
		c();
	}

	followListC(npcList, callback) {
		// walks path rather than recall search
		let finishedCallback = [false]; // it's just one thread
		let char = this;
		let npc = npcList[0];
		let began = [false];
		if (!npc) {char.return(); return;}
		char.following = npc; //
		let followId = uid();
		char.followId = followId;

		if (Array.isArray(callback)) callback = callback.shift();
		function c (){
			if (char.followId !== followId) return;
			if (!char.followMode) {return;}
// && distance(char.selectedRow, char.following.row, char.selectedCol, char.following.col) > char.arrivalRange
			
			// let dist = distance(char.row, char.following.row, char.col, char.following.col)
			// if (began[0] && dist > char.arrivalRange) { // doesn't recall for long distances
			// 	setTimeout(c, char.speed);
			// 	return;
			// } else if (dist < 2) {
			// 	setTimeout(c, char.speed);
			// 	return;
			// }

			let nearestLib = nearestLiberty(char.row, char.col, char.following.row, char.following.col)
			char.go(nearestLib.row, nearestLib.col)
			began[0] = true;
			if (distance(char.row, char.selectedRow, char.col, char.selectedCol) <= char.followingRange) {
				if (!finishedCallback[0]) {
					finishedCallback[0] = true;
					callback();
				}
			}
		
			// if (char.debugFollow) {
			// 	console.log('looping c - '+char.selectedRow+" - "+char.selectedCol)
			// 	console.log('done - '+char.done + ", interrupted - "+char.interrupted)
			// }
			setTimeout(c, char.speed); // how to synchronize
		}
		c();
	}

	

	panic(enemy) {
		this.run();
		// use a greedy euclidean opposite algorithm
	}

	unpanic() {

	}

	return(){
		this.standby();
		this.walk();
		let npc = this;
		function heal(){npc.live();}
		this.go(this.heartScript.row, this.heartScript.col, this.makeTraverse(heal));
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	

	standby() {
		this.lockId = uid();
		this.chaseId = null;
		//this.popId = uid();
		//this.sceneId = uid();
		this.pathSearch = makeWallSearch(this);

		this.followRange = this.trueFollowRange;
		this.callbackList = [];
		this.baseSpeed = this.trueBaseSpeed;
		this.walk();
		this.traverseLoop = false;
		this.cutsceneMode = false;
		this.silent = false;
		//this.silenceToken = false;
		this.attackMode = false;
		this.followMode = false;
		this.fetchMode = false;
		this.stopLoop = true;
		this.heartScript.appendColorTile(this.heartColor);
		//if(!this.done) return;

		
		if (this.responsive){
			if (this.heartScript.successText && this.heartScript.successText.length > 0)
				this.flavorText = this.heartScript.successText;
			else this.flavorText = this.heartScript.closeText;
			this.open(this.airtime);
		} else {
			this.forceClose() // test alice le brambot
		}
	}

	silence() {
		this.silent = true;
	}


	processOutput(result) {this.heartScript.activatedCondition(this, result);}

	processOutputScript(result, heartScript) {
		// live if you collect their heartScript as their faction successfully
		// die if you collect their heartScript as opposing faction
		// first breakthrough - OutputNPC
		// if you collect their file, they show popover
			// how to show popover

		if (!result['script'] || (heartScript.id !== result['script']['id']))
			return;

		switch(result['has_heart']){
			case false: this.flavorText = heartScript.failText; break;
			case true: this.flavorText = heartScript.successText; this.changeForm(); break;
		} // activation

	}


	updateFlavorText() {
		this.flavorText = this.heartScript.closeText;
		if (this.form === "ghost")
			this.flavorText = '';
		// return flavorText to default
			// or ghost
			// or panic
	}
}

class Boss extends Civilian {
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList);

		this.species = 'boss';
		this.subspecies = 'boss';
		this.truespecies = 'boss';
		this.truesubspecies = 'boss';


		this.goodMessages = [];
		this.badMessages = [];
		this.messages = this.goodMessages;
		this.msgIdx = 0;
		this.attackMode = false;

		this.cutsceneMode = false;
		this.airtime = 3000;
		this.delay = 20000;
		this.silent = false;

	}



	chaseList(npcList) { // what about when list runs out? Needs a wait a react protocol
		let boss = this;
		let chaseId = uid();
		if (boss.chaseId) return;
		boss.chaseId = chaseId 
		let began = [false]
		function c (){
			if (chaseId !== boss.chaseId) return;
			let npc = npcList[0];
			if (!npc) {
				boss.return();
				return; // stops here - goes back to heart file in standby mode
			}
			if (!boss.attackMode) {return;}

			let dist = distance(boss.row, boss.selectedRow, boss.col, boss.selectedCol)
			if (began[0] && dist > boss.arrivalRange) { // doesn't recall for long distances
				setTimeout(c, boss.speed);
				return;
			}
			let path = greaterWallSearch(boss.row, boss.col, npc.row, npc.col);
			if (path.length === 0) {return;}
			boss.go(path[path.length-1].row, path[path.length-1].col);
			began[0] = true;
			////////////////////////////////////////////////////////////
			if (path.length === 1) {npc.toGhost(); npcList.shift(); began[0] = false;} // ghost and doesn't stop yet
			setTimeout(c, boss.speed);
		}
		c();
	}


	toggleMessage() {
		this.msgIdx = (this.msgIdx + 1) % this.goodMessages.length;
		if (this.attackMode) this.messages = this.badMessages;
		else this.messages = this.goodMessages;
		this.flavorText = this.messages[this.msgIdx % this.messages.length]
	}


	attack(target) {
		if (target.length === 0) return;
		if (this.isGhost()) return;
		this.standby(); 
		this.run();
		this.attackMode = true;
		let boss = this;
		
		let first = target.shift();
		let targets = window.characterDict[first].slice(0);
		targets.sort(function(a, b){
			return distance(boss.row, a.row, boss.col, a.col) - distance(boss.row, b.row, boss.col, b.col)
		})

		this.heartScript.removeColorTile();
		this.heartScript.appendColorTile('darkred');
		this.chaseList(targets);
		//this.loopOpenDefault();

	}

	loopOpenDefault() {
		if (this.stopLoop)
			this.loopOpen(this.airtime, this.delay)
	}


	reengage() {
		this.live();
		this.return();
		//this.speak();
	}

	return(callback) {
		this.standby();
		if (callback && Array.isArray(callback)) callback = callback[0];
		let npc = this;
		function heal(){npc.live();}
		this.go(this.heartScript.row, this.heartScript.col + 1, heal);
	}


	speak () {
		//this.silent = false;
		//this.loopOpenDefault();
	}
	

	updateFlavorText() {
		if (this.silent) {this.flavorText = ''; return;}
		let messageArray;
		if (this.attackMode) messageArray = this.badMessages;
		else messageArray = this.goodMessages;
		if (this.goodMessages.length === 0)
			this.flavorText = this.heartScript.closeText;
		else{
			this.flavorText = messageArray[this.msgIdx];
			this.msgIdx = (this.msgIdx + 1) % this.goodMessages.length;
		}
	}

}

function getBram(){return window.characterDict['brambot'][0];}


class Critter extends Civilian {
	constructor(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList) {
		super(character, imgCellWidth, imgCellHeight,
    			basePose, facing, baseSpeed, mode, heartScript, n, getFurthest, scriptList);

		this.species = 'critters';
		this.subspecies = 'critters';
		this.truespecies = 'critters';
		this.truesubspecies = 'critters';
		this.critterBuff = 3;


		this.heartColor = '#eb93ff'
		this.heartScript.appendColorTile("#eb93ff");
		this.befriend = true;
	}
}