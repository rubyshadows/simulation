class NPC extends Character {
	constructor(character, imgCellWidth, imgCellHeight,
    			row, col, basePose, facing, baseSpeed, mode, flavorText) {

		super(character, imgCellWidth, imgCellHeight,
    		  row, col, basePose, facing, baseSpeed, mode, window.map);

		this.pathSearch = makeWallSearch(this);
		this.user = window.user;
		this.user.subscribers.push(this);

		this.flavorText = flavorText;
		this.minRange = 1.5;
		this.maxRange = 100;
		this.delayOpen = 3200000; // Hour of silence
		this.airtime = 3000;
		this.conversationPace = CIVILIANSPEED() * 10;
		this.silenceToken = false;

		this.setPopover('left');
		this.closed = true;
	}

	getSide(side) {
		let dirs = ["up", "right", "down", "left"];
		let popDirs = ["top", "right", "bottom", "left"];
		let facingIdx = dirs.indexOf(this.facing.toLowerCase());
		let rightIdx = (facingIdx + 1) % 4;
		let backIdx = (facingIdx + 2) % 4;
		let leftIdx = (facingIdx + 3) % 4;
		switch(side){
			case "up": return this.nextSide(popDirs[facingIdx], 5);
			case "right": return this.nextSide(popDirs[rightIdx], 5);
			case "back": return this.nextSide(popDirs[backIdx], 5);
			case "left": return this.nextSide(popDirs[leftIdx], 5);
		}
	}

	getUserOppositeSide() {
		let user = window.user;
		let placement = euclideanOpposite(this.row, this.col, user.row, user.col);
		let dirs = ["up", "right", "down", "left"];
		let popDirs = ["top", "right", "bottom", "left"];
		let facingIdx = dirs.indexOf(placement.toLowerCase());
		return this.nextSide(popDirs[facingIdx], 5)
	}

	nextSide(popDir, range, depth){
		// check for barrier at range 5
		if (depth === undefined)
			depth = 4;
		if (depth === 0)
			return popDir;

		let dirs = ["up", "right", "down", "left"];
		let popDirs = ["top", "right", "bottom", "left"];

		let position = new Position(this, this.row, this.col, dirs[popDirs.indexOf(popDir)], range);
		position = position.forward();
		let nextIdx = (popDirs.indexOf(popDir) + 1) % 4;
		for (let i = 0; i < range; i++) {
			if (position.row < 0 || position.col < 0 || position.row >= window.map.mapRows || position.col >= window.map.mapCols)
				return this.nextSide(popDirs[nextIdx], range, depth - 1);
			position = position.forward();
		}
		return popDir;
	}

	setPopover(placement) {
		let char = this;

		$(this.charDiv).popover({
			trigger: 'manual',
		    animation: true,
		    html: true,
		    placement: char.getSide(placement),
		    title: function() {
	      		return char.character.slice(0,1).toUpperCase() + char.character.slice(1).toLowerCase();
	    	},
	    	content: function() {
	    		// civilian class changes flavortext
	      		return char.flavorText;
	    	}
		});

	}

	resetPopover(placement){

		$(this.charDiv).popover('destroy');
		this.setPopover(placement);
	}

	closeCutscene(){
		if (this.cutsceneMode) 
			{
				if(this.silenceToken) {
					this.silent = true;
					this.silenceToken = false;
				}
				this.forceClose(); 
			}
	}

	forceClose(){
		// if (this.character === 'ju')
		// 	console.log('force close script #' + this.idx)
		if (!this.closed)
			this.resetPopover('back');
		this.minRangeRetract();
		this.closed = true;
		this.locked = false;
		// $(this.charDiv).popover('hide'); 
	}

	forceOpen(){
		if (this.flavorText.length === 0){
			this.forceClose();
			return;
		}

		if (!this.closed){
			this.setPopover('back');
			$(this.charDiv).popover('show');
		}
		else {
			this.setPopover('back');
			$(this.charDiv).popover('show');
		}
		this.closed = false;
		this.minRangeExpand();
	}

	softOpen(){
		if (this.flavorText.length === 0){
			this.forceClose();
			return;
		}

		this.setPopover('back');
		$(this.charDiv).popover('show');
		this.closed = false;
		this.minRangeExpand();
	}

	inRange(char){
		return distance(this.row, char.row, this.col, char.col) <= this.minRange;
	}

	minRangeExpand(){this.minRange = 2;}
	minRangeRetract(){this.minRange = 1.5;}

	popClose(char, time) {
		function c(){$(char.charDiv).popover('hide');  char.closed = true;}
		setTimeout(c, time);
	}

	popKill(time, callback) {
		let npc = this;
		function c(){
			console.log("popkill")
			npc.forceClose()
			callback();
		}
		
		setTimeout(c, time);
	}

	popOpen(time, stayOpen){
		//this.resetPopover('back');
		let popId = uid();
		let npc = this;
		let selectedCoordinates = [npc.selectedRow, npc.selectedCol]; // pick direction based on coordinates
		npc.popId = popId;
		npc.cutsceneMode = true;
		npc.locked = true;
		this.softOpen();
		function c(){
			if (!npc.locked || !npc.cutsceneMode || npc.arrived) return; // it doesn't crush navi message
			if (npc.popId !== popId) return;
			npc.locked = false;
			if (!npc.closed){
				// if (npc.character === 'ju'){
				// 	console.log("popOpen closing script #: "+npc.idx)
				// 	console.log("popOpen arrived is "+npc.arrived+" - closed is "+npc.closed+".")
				// }
				npc.forceClose()
			}
			npc.closed = true;
		}
		
		setTimeout(c, time);
	}
////////////////////////////////////////////////////////////////////////////////////
	process(char){
		if (this.silent) this.forceClose();
		if (this.locked || this.silent || !this.responsive) return;
		if (this.inRange(char)) {
			if (this.updateFlavorText)
				this.updateFlavorText();
			this.openSilence(this.airtime); // yep
			if (this.character === 'ju')
					console.log(this.character + ": open silence")
		}

	}

	npcUpdate(char) {
		if (this.silent)
			this.process(char);
		if(this.locked && !this.closed) 
			this.softOpen();
		else 
			this.process(char);
	}
///////////////////////////////////////////////////////////////////////////////////////
	scene(on){
			this.cutsceneMode = on;
			this.locked = on;
			this.interruptable = !on;
	}

	openSilence(airtime) {
		this.softOpen(); // for when bot isn't updating
		let lockId = uid();
		this.lockId = lockId;
		let npc = this;
		npc.locked = true;
		function c(){
			if (npc.lockId !== lockId) return;
			npc.locked = false;
			npc.silent = true
			npc.forceClose();
		}
		setTimeout(c, airtime);
	}

	loopOpen(airtime, delay) { // will use in civilian, by time killing it
		let npc = this;
		let loopId = uid();
		npc.loopId = loopId;
		this.stopLoop = false;
		this.locked = true;
		function o(){
			if (npc.loopId !== loopId) return;
			if (npc.stopLoop) {npc.locked = false; return;}
			npc.toggleMessage();
			npc.open(airtime)
			setTimeout(o, airtime + delay);
		}
		o();
	}

	lockOpen(time, callback) {
		this.softOpen(); // for when bot isn't updating
		let lockId = uid();
		this.lockId = lockId;
		let npc = this;
		npc.locked = true;
		function c(){
			if (npc.lockId !== lockId) return;
			npc.cutsceneMode = true;
			npc.locked = false;
			if (callback) callback();
		}
		setTimeout(c, time);
	}

	open(time, callback){// closes automatically
		this.softOpen(); 
		let lockId = uid();
		this.lockId = lockId;
		let npc = this;
		npc.locked = true;
		function c(){
			if (npc.lockId !== lockId) return;
			npc.forceClose();
			npc.locked = false;
			if (callback) callback();
		}
		setTimeout(c, time);
	}

	uninterruptablePopOpen(time, stayOpen){
		let npc = this;
		let selectedCoordinates = [npc.selectedRow, npc.selectedCol];
		npc.locked = true;
		function c(){
			npc.locked = false;
			npc.closed = false;
			if (!stayOpen){
				if (npc.character === 'ju')
					console.log("popOpen closing")
				//npc.forceClose()
			}
		}
		
		setTimeout(c, time);
	}
}

function cutScene(npc, messages, spaceDelay, path, callback){
	let totalTime = npc.speed * path.length;
	let fraction = totalTime / messages.length;
	let airtime = fraction - spaceDelay;
	let selectedCoordinates = [npc.selectedRow, npc.selectedCol];
	let messagesCopy = messages.slice(0);
	let sceneId = uid();
	npc.sceneId = sceneId;
	npc.scene(true);
	
	function unlock(time, callback) {
		npc.locked = true;
		function u(){
			//npc.scene(false) - arrival function must do this
			if(callback && sceneId === npc.sceneId) callback();
		}
		setTimeout(u, time);
	}

	function play(){
		if (npc.sceneId !== sceneId) return; 
		let msg = messagesCopy.shift();
		if (msg) {
			npc.flavorText = userInject(msg);
			npc.scene(true);
			if (messagesCopy.length > 0){
				npc.popOpen(airtime);
				setTimeout(play, airtime + spaceDelay);
			}
			else{
				npc.lockOpen(airtime); // the last message
			}
			
		}
		else 
			npc.scene(false)

	}

	play();


	// for each message, wait for delay, then show message 
}