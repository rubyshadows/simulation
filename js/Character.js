class Character {
	constructor(character, imgCellWidth, imgCellHeight,
	            row, col, basePose, facing, baseSpeed, mode, map, search) {

		if (window.characterCount !== undefined)
			window.characterCount++;
		else
			window.characterCount = 1;
		this.ID = character+"-"+characterCount; 

		this.charDiv = undefined; 
		this.charImg = undefined;
		this.imgId = this.ID + "Img";
		this.map = map;     

		this.location = map.location;
		this.character = character;
		this.form = this.character;
		this.row = row; 
		this.col = col;

		this.selectedRow = row; 
		this.selectedCol = col;
		this.facing = facing; 
		this.mode = mode;
		this.basePose = basePose;
		this.pose = basePose + facing;
		this.lastPose = this.pose;
		this.baseSpeed = baseSpeed; 
		this.speed = baseSpeed;
		this.runMultiplier = 1.5
		this.trueBaseSpeed = baseSpeed;
		this.left = col * map.cellSize; 
		this.top = row * map.cellSize;  
		this.imgFolder = "images/characters/";

		this.imgCellWidth = imgCellWidth; 
		this.imgCellHeight = imgCellHeight;
		this.pathSearch = search;
		this.done = true;
		this.path = [];
		this.destinations = [];

		this.display = undefined;

		this.subscribers = [];

		this.interruptable = true;
		this.species = 'collectors';
		this.callbackList = [];

		this.counter = 0;

		this.species = 'collectors'
		this.subspecies = 'collectors'
		this.truespecies = 'collectors'
		this.truesubspecies = 'collectors'

		//set in map
		this.map.enter(this);
		//create character object
		let gb = document.getElementById("gamebox"); 
		let charDiv = document.createElement("div"); this.charDiv = charDiv;
		let img = document.createElement("img"); this.charImg = img;
		charDiv.character = this;
		charDiv.append(img); gb.append(charDiv)
		charDiv.setAttribute("id", this.ID); charDiv.setAttribute("class", "character");
		setValues(charDiv, this.left, this.top, 
				  this.imgCellWidth * this.map.cellSize, 
				  this.imgCellHeight * this.map.cellSize);
		charDiv.style["z-index"] = this.row;
		charDiv.style.transition = "left "+this.speed+"ms linear, top "+this.speed+"ms linear";
		img.setAttribute("src", this.imgFolder+this.form+"/"+this.form+this.pose+".gif"); 
		img.setAttribute("id", this.imgId);

	}

	live(){this.unGhost()}
	run() {this.mode = "Run"}
	walk() {this.mode = "Walk"}

	changeID(id){
		this.ID = id;
		this.charDiv.setAttribute("id", this.ID);
		this.imgId = id+"Img";
		this.charImg.setAttribute("id", this.imgId);
	}

	changeForm(ghost) {
		if (ghost){
			this.toGhost();
		}
		else {
			this.unGhost();
		}
	}

	face() {
		let row = this.row;
		let col = this.col;
		let facing = euclideanFace(row, col, this.selectedRow, this.selectedCol);
		this.updateC(row, col, facing);
		faceStill(this, this.facing);
		this.done = true;
	}

	faceAuto(dir) {
		let facing = dir.charAt(0).toUpperCase() + dir.slice(1);
		this.facing = facing;
		this.updateC(this.row, this.col, this.facing);
		faceStill(this, this.facing);
		this.done = true;
	}


	found() {
		return distance(this.row, this.selectedRow, this.col, this.selectedCol) === 0;
	}

	toGhost() {
		if (window.user !== this) this.baseSpeed = GHOSTSPEED();
		this.form = "ghost";
		this.subspecies = "ghosts";
		this.update(this.row, this.col)

	}
	unGhost() { // may not need
		this.baseSpeed = this.trueBaseSpeed
		this.form = this.character;
		this.subspecies = this.truesubspecies;
		this.update(this.row, this.col)
	}

	isGhost(){ return this.form === 'ghost';}

	goOld(row, col, callback){
		this.selectedRow = row, this.selectedCol = col;
		if (check(window.map, row, col)){
			this.done = false;
			this.pathSearch(callback);
		} else if (callback) callback();
	}

	go(row, col, callback){
		this.callback = callback;
		if (check(window.map, row, col)){
			this.selectedRow = row, this.selectedCol = col;
			if (this.done){
				this.done = false;
				this.pathSearch(callback);
			}
		} else if (callback) callback();
	}

	pingSubscribers(){
		for (let subscriber of this.subscribers)
			subscriber.process(this);
	}

	pingOutputSubscribers(result){
		for (let subscriber of this.subscribers)
			if (subscriber.processOutput)
				subscriber.processOutput(result);
	}

	update(destRow, destCol, facing, moving){
		// pathSearch calculates destRow and destCol, but changes nothing
		
		if (facing)
			this.facing = facing;
		else {
			if (this.col < destCol) this.facing = "Right";
			else if (this.col > destCol) this.facing = "Left";
			else if (this.row > destRow) this.facing = "Up";
			else if (this.row < destRow) this.facing = "Down";
		}

		this.moving = moving;
		if (moving === undefined)
			this.moving = this.facing;

		this.interrupted = false;
		if (window.user.row === destRow && window.user.col === destCol) { // good // might collide with wall search
			this.done = true;
			this.interrupted = true && this.interruptable;
		}

		if (this.row === destRow && this.col === destCol){
			this.done = true;
			this.interrupted = false;
		}

		// if (this.col < destCol) this.facing = "Right";
		// else if (this.col > destCol) this.facing = "Left";
		// else if (this.row > destRow) this.facing = "Up";
		// else if (this.row < destRow) this.facing = "Down";  // must change // get facing from input
		// else this.done = true; // is good // if at location done is true

		
		else{
			let check = this.map.items[destRow+"-"+destCol];
			if (check && check[user.ID]) {
				this.done = true;
				this.interrupted = true;
			}
		}


		if (this.mode === "Run") this.speed = this.baseSpeed / this.runMultiplier;
		else this.speed = this.baseSpeed;
		this.lastPose = this.pose;
		this.pose = this.mode + this.facing; // good

		if(this.done === false){ // good
			this.map.leave(this);
			switch(this.moving){ // must change // unless it's some kind of walk algorithm // doesnt reach destination
				// but it shouldn't reach destination
				case "Right": this.col += 1; break;
				case "Left": this.col -= 1; break;
				case "Up": this.row -= 1; break;
				case "Down": this.row += 1; break;
			}	
			this.map.enter(this);

		} else this.pose = this.basePose + this.facing // good
		// reposition

		this.reposition();
		this.pingSubscribers(); // the user pings us // we don't need to process when we move // thats npcupdate

		//if (this.process) this.process(window.user); 
		if (this.npcUpdate) this.npcUpdate(window.user); // still process when done?

	}

	reposition(callback){
		let char = this;
		function zUpdate() {
			let buff = 0;
			if (char.critterBuff) buff = char.critterBuff
			char.charDiv.style["z-index"] = (char.row - buff);
		}
		this.left = this.col * this.map.cellSize;
		this.top = this.row * this.map.cellSize;
		this.charDiv.style.transition = "left "+this.speed+"ms linear, top "+this.speed+"ms linear";
		this.charDiv.style.left = this.left + "px";
		this.charDiv.style.top = this.top + "px";
		//this.charDiv.style["z-index"] = this.row;

		if (this.facing === 'Down') this.charDiv.style["z-index"] = this.row;
		else setTimeout(zUpdate, this.speed)


		let src = this.imgFolder+this.form+"/"+this.form+this.pose+".gif" ///// make simpler 
		let oldsrc = this.charImg.getAttribute("src");
		if (src !== oldsrc && !this.done)
			this.charImg.setAttribute("src", src);
		

		function stopUpdating() {
			if (char.row === char.selectedRow && char.col === char.selectedCol){
				char.done = true;
				char.interrupted = false;
			} else char.done = false;

			if (char.done || char.interrupted) faceStill(char, char.facing);
			char.updating=false;

			if(callback) callback();
		}
		setTimeout(stopUpdating, this.speed); // set done
		
	}


	update2(destRow, destCol, facing, moving, callback){
		// pathSearch calculates destRow and destCol, but changes nothing
		if (facing)
			this.facing = facing;
		else {
			if (this.col < destCol) this.facing = "Right";
			else if (this.col > destCol) this.facing = "Left";
			else if (this.row > destRow) this.facing = "Up";
			else if (this.row < destRow) this.facing = "Down";
		}

		this.moving = moving;
		if (moving === undefined)
			this.moving = this.facing;

		this.interrupted = false;
		if (window.user.row === destRow && window.user.col === destCol) { // good // might collide with wall search
			this.done = true;
			this.interrupted = true && this.interruptable;
		}

		let char = this;
		function stopUpdating() {if (char.done) faceStill(char, char.facing); char.updating=false;}
		if (this.row === destRow && this.col === destCol){ // safe search cant use selectedRow,selectedCol
			this.done = true;
			this.interrupted = false;
			//stopUpdating();
			//return;
		}

		// if (this.col < destCol) this.facing = "Right";
		// else if (this.col > destCol) this.facing = "Left";
		// else if (this.row > destRow) this.facing = "Up";
		// else if (this.row < destRow) this.facing = "Down";  // must change // get facing from input
		// else this.done = true; // is good // if at location done is true

		
		else{
			let check = this.map.items[destRow+"-"+destCol];
			if (check && check[user.ID]) {
				this.done = true;
				this.interrupted = true;
			}
		}


		if (this.mode === "Run") this.speed = this.baseSpeed / this.runMultiplier;
		else this.speed = this.baseSpeed;
		this.lastPose = this.pose;
		this.pose = this.mode + this.facing; // good

		if(this.done === false){ // good
			this.map.leave(this);
			switch(this.moving){ // must change // unless it's some kind of walk algorithm // doesnt reach destination
				// but it shouldn't reach destination
				case "Right": this.col += 1; break;
				case "Left": this.col -= 1; break;
				case "Up": this.row -= 1; break;
				case "Down": this.row += 1; break;
			}	
			this.map.enter(this);
			this.pose = this.mode + this.facing
			// if (this.character === 'ju' && this.idx === 2) {
			// 	console.log('update updating to: ' + this.row + " - " +this.col)
			// 	window.map.floorTileImg = "images/map/redroomfloor.png"
			// 		let tile = window.map.layTile(this.row, this.col,1.5);
			// 		tile.style['z-index'] = 1000;
			// 		tile.style['opacity'] = .3;
			// 	window.map.floorTileImg = "images/map/roomfloor.png"
			// }

		} else this.pose = this.basePose + this.facing // good
		// reposition

		if (this.row === this.selectedRow && this.col === this.selectedCol){
			this.done = true;
			this.interrupted = false;
		}

		

		this.reposition(callback);
		this.pingSubscribers(); // the user pings us // we don't need to process when we move // thats npcupdate

		//if (this.process) this.process(window.user); 
		if (this.npcUpdate) this.npcUpdate(window.user); // still process when done?

	}

	updateC(destRow, destCol, facing, moving, callback) {
		if (facing)
			this.facing = facing;
		else {
			if (this.col < destCol) this.facing = "Right";
			else if (this.col > destCol) this.facing = "Left";
			else if (this.row > destRow) this.facing = "Up";
			else if (this.row < destRow) this.facing = "Down";
		}


		this.moving = moving;
		if (!moving) this.moving = this.facing;

		this.interrupted = false;
		if (window.user.row === destRow && window.user.col === destCol) { // good // might collide with wall search
			this.done = true;
			this.interrupted = true;
		}

		let char = this;
		function stopUpdating() {if (char.done) faceStill(char, char.facing); char.updating=false;}
		if (this.row === destRow && this.col === destCol){ // safe search cant use selectedRow,selectedCol
			this.done = true;
			//this.interrupted = false;
			//stopUpdating();
			//return;
		}

		// if (this.col < destCol) this.facing = "Right";
		// else if (this.col > destCol) this.facing = "Left";
		// else if (this.row > destRow) this.facing = "Up";
		// else if (this.row < destRow) this.facing = "Down";  // must change // get facing from input
		// else this.done = true; // is good // if at location done is true


		if (this.mode === "Run") this.speed = this.baseSpeed / this.runMultiplier;
		else this.speed = this.baseSpeed;
		this.lastPose = this.pose;
		this.pose = this.mode + this.facing; // good

		if(this.done === false){ // good
			this.map.leave(this);
			switch(this.moving){ // must change // unless it's some kind of walk algorithm // doesnt reach destination
				// but it shouldn't reach destination
				case "Right": this.col += 1; break;
				case "Left": this.col -= 1; break;
				case "Up": this.row -= 1; break;
				case "Down": this.row += 1; break;
			}	
			this.map.enter(this);
			this.pose = this.mode + this.facing
			// if (this.character === 'ju' && this.idx > 5) {
			// 	console.log('update updating to: ' + this.row + " - " +this.col)
			// 	window.map.floorTileImg = "images/map/redroomfloor.png"
			// 		let tile = window.map.layTile(this.row, this.col,1.5);
			// 		tile.style['z-index'] = 1000;
			// 		tile.style['opacity'] = .3;
			// 	window.map.floorTileImg = "images/map/roomfloor.png"
			// }

		} else this.pose = this.basePose + this.facing // good
		// reposition

		

		this.reposition(callback);
		this.pingSubscribers(); // the user pings us // we don't need to process when we move // thats npcupdate

		//if (this.process) this.process(window.user); 
		if (this.npcUpdate) this.npcUpdate(window.user); // still process when done?
	}

}

function faceStill(char, dir) {
	if(this.debug) console.log('faceStill')
	char.facing = dir
	char.pose = char.basePose + char.facing
	let src = char.imgFolder+char.form+"/"+char.form+char.pose+".gif"
	let oldsrc = char.charImg.getAttribute("src");
	if (src !== oldsrc)
		char.charImg.setAttribute("src", src);
}

function makeSimpleSearch(char){
	function search(){
		let row = char.row;
		let col = char.col;
		if (char.done) return;
		if (char.selectedRow > row 		&& check(char.map, row+1, col)) row += 1;
		else if (char.selectedRow < row && check(char.map, row-1, col)) row -= 1;
		else if (char.selectedCol > col && check(char.map, row, col+1)) col += 1;
		else if (char.selectedCol < col && check(char.map, row, col-1)) col -= 1;
		else {char.update(row, col); return;}
		char.update(row, col);
		setTimeout(char.pathSearch, char.speed);
	}
	return search;
}

function makeSimpleSearchPlus(char){
	function search(){
		if (char.row === char.selectedRow && char.col === char.selectedCol)
			char.update(char.row, char.col);

		if (char.done) return;
		//console.log("making position: "+char.facing)
		let position = new Position(char, char.row, char.col, char.facing, 100);
		let choice = position.euclideanDecide();
		if (choice === undefined || position.distance === 0){
			char.update(char.row, char.col);
			return;
		}
		else{ // when we've succeeded done is set to true.
			// search nest: simplePlus > wallSearch > simplePlus - later
			char.update(choice.row, choice.col);
			setTimeout(char.pathSearch, char.speed);
		}
	}

	return search;
}

function makeUserSearch(char) {
	function search(callback) {
		let path = greaterWallSearch(char.row, char.col, char.selectedRow, char.selectedCol);
		let run = distance(char.row, char.selectedRow, char.col, char.selectedCol) > 30;
		if (run) char.run();
		else char.walk();
		//char.path = getPath(char.row, char.col, char.selectedRow, char.selectedCol);
		if (path.length > 0)
			WalkPathRecursiveCallback(char, path);
		else
			char.done = true;
	}

	return search;
}

function makeFollowSearch(char) {
	function search(callback) {
		//char.callbackList.push(callback);
		let path = greaterWallSearch(char.row, char.col, char.selectedRow, char.selectedCol);
		WalkPathRecursiveCallback(char, path, callback);
	}

	return search;
}

function makeWallSearch(char) {

	function search(callback) {
		let path = greaterWallSearch(char.row, char.col, char.selectedRow, char.selectedCol);
		//char.path = getPath(char.row, char.col, char.selectedRow, char.selectedCol);
		if (path.length > 0)
			WalkPathRecursiveCallback(char, path, callback);
	}

	return search;
}

function makeNaviSearch(navi) {
	function faceScript(){
		navi.selectedCol = navi.currentScript().col;
		navi.selectedRow = navi.currentScript().row;
		navi.face();
		navi.selectedCol = navi.col;
		navi.selectedRow = navi.row;
		//console.log("navi face launched")
		navi.currentScript().arrivedFunction(navi);
	}
	function search(callback) {
		let script = navi.currentScript();
		let nearestLib = nearestLiberty2D(navi.row, navi.col, script.row, script.col);
		//console.log("search: liberty: "+nearestLib.col + ' - ' +nearestLib.row)
		let path = greaterWallSearch(navi.row, navi.col, nearestLib.row, nearestLib.col);
		function arrivedFunction() {
			faceScript();
			//console.log('navi done: '+navi.done)
		}
		if (path.length > 0){
			navi.selectedCol = nearestLib.col
			navi.selectedRow = nearestLib.row
		}
		navi.callbackList = []; // each new go clears it - interruptable without colliding with old cutscenes
		// navi.callbackList.push(faceScript);
		// navi.callbackList.push(arrivedFunction);
		// navi.callbackList.push(callback);
		WalkPathRecursiveCallback(navi, path, arrivedFunction);
	}
	return search;
}

function makeScriptSearch(char) {
	function faceScript(){
		char.face();
		script.arrivedFunction(char);
	}
	function search(callback) {
		let script = char.currentScript();
		let nearestLib = nearestLiberty(char.row, char.col, script.row, script.col)
		let path = greaterWallSearch(char.row, char.col, nearestLib.row, nearestLib.col);
		function arrivedFunction() {
			faceScript();
		}
		if (path.length > 0){
			char.selectedCol = nearestLib.col
			char.selectedRow = nearestLib.row
		}
		char.callbackList = []; // each new go clears it - interruptable without colliding with old cutscenes
		// char.callbackList.push(faceScript);
		// char.callbackList.push(arrivedFunction);
		// char.callbackList.push(callback);
		WalkPathRecursiveCallback(char, path, arrivedFunction);
	}
	return search;
}

function appendSearch(myChar, search) {
	myChar.pathSearch = makeWallSearch(myChar);
	switch(search) {
		case "wallSearch":myChar.pathSearch = makeWallSearch(myChar); break;
		case "simpleSearch":myChar.pathSearch = makeSimpleSearch(myChar); break;
		case "simpleSearchPlus":myChar.pathSearch = makeSimpleSearchPlus(myChar); break;
		case "safeSearch":myChar.pathSearch = makeSafeSearch(myChar); break;
		case "scriptSearch":myChar.pathSearch = makeScriptSearch(myChar); break;
		case "naviSearch":myChar.pathSearch = makeNaviSearch(myChar); break;
		case "userSearch":myChar.pathSearch = makeUserSearch(myChar); break;
		case "followSearch":myChar.pathSearch = makeFollowSearch(myChar); break;
	}
}

function makeCharacter(map, row, col, character, search){

	let imgCellWidth = 1, 
	imgCellHeight = 1,  
	baseSpeed=USERSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new Character(character, imgCellWidth, imgCellHeight,
    				           row, col, basePose, facing, baseSpeed, mode, map, null);
	appendSearch(myChar, search)

	return myChar;
}

function makeAlice(character, search, heartScript, n, getFurthest) {
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=CRITTERSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new Alice(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, n, getFurthest);
	appendSearch(myChar, search)

	return myChar;
}

function makeBoss(character, search, heartScript, n, getFurthest){
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=BOSSSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new Boss(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, n, getFurthest);
	appendSearch(myChar, search)

	return myChar;
}

function makeCivilian(character, search, heartScript, n, getFurthest){
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=CIVILIANSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new Civilian(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, n, getFurthest);
	appendSearch(myChar, search)

	return myChar;
}

function makeNPC(row, col, character, flavorText, search){
	let imgCellWidth = 1, 
	imgCellHeight = 1,  
	baseSpeed=CIVILIANSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new NPC(character, imgCellWidth, imgCellHeight,
    				           row, col, basePose, facing, baseSpeed, mode, flavorText);
	//myChar.pathSearch = makeSimpleSearch(myChar);
	appendSearch(myChar, search)

	return myChar;
}

function makeScriptCollector(character, search, heartScript, n, getFurthest) {
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=CIVILIANSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new ScriptCollector(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, n, getFurthest);
	appendSearch(myChar, search)

	return myChar;
}


function makeNetNavi(character, heartScript, scriptList) {
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=NAVISPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new NetNavi(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, scriptList.length, false, scriptList);
	appendSearch(myChar, "naviSearch");
	return myChar;
}

function makeCritter(character, search, heartScript, n, getFurthest) {
	let imgCellWidth = 1,
	imgCellHeight = 1,  
	baseSpeed=CRITTERSPEED(), 
	basePose="Still",
	facing="Down", 
	mode="Walk";
	let myChar = new Critter(character, imgCellWidth, imgCellHeight,
    								 basePose, facing, baseSpeed, mode,
    								 heartScript, n, getFurthest);
	appendSearch(myChar, search)

	return myChar;
}

function makeTitan(map, row, col){

	let character=name,
	imgCellWidth = 1, 
	imgCellHeight = 1,  
	baseSpeed=USERSPEED(), 
	basePose="Still",
	facing="Right", 
	mode="Walk";
	let myChar = new Character(character, imgCellWidth, imgCellHeight,
    				           row, col, basePose, facing, baseSpeed, mode, map, null);
	myChar.pathSearch = makeSimpleSearch(myChar);

	return myChar;
}

function makeSimpleLoiter (axis, myChar, rangeLow, rangeHigh) {
	function loiter(){
		let data = {row: {dir1: "Down", dir2: "Up"}, col:{dir1: "Right", dir2:"Left"}}
		let inc = 1;
		if (myChar[axis] < rangeHigh && myChar.facing === data[axis].dir1) inc = inc;
		else if (myChar[axis] >= rangeHigh) inc = -inc;
		else if (myChar[axis] > rangeLow && myChar.facing === data[axis].dir2) inc = -inc;
		else if (myChar[axis] <= rangeLow) inc = inc;
		
		if (axis === "row")
			myChar.go(myChar.row + inc, myChar.col);
		else
			myChar.go(myChar.row, myChar.col + inc);
	}

	return loiter;
}

function makeLoiterer(l, frequency, wait, map){ 
	//currently only makes titans
	let row = l.row, col = l.col, axis = l.axis, rangeLow = l.rangeLow, rangeHigh = l.rangeHigh;
	function loiterTitan() {
		let titan = makeTitan(map, row, col);
		function waitTime(){
			setInterval(makeSimpleLoiter(axis, titan, rangeLow, rangeHigh), 
			frequency);
		}
		setTimeout(waitTime, wait);
	}
	return loiterTitan;
}

