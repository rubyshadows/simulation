class ActionModal {
	constructor() {
		window.actionModal = this;
		this.actionButtons = document.getElementById("actionButtons");
		this.actionContent = document.getElementById("actionContent");
		this.actionForm = document.getElementById("actionForm");

		this.actionTitle = document.getElementById("actionTitle");
		this.actionTitleImg = document.getElementById("actionTitleImg");

		this.actionLoading = document.getElementById("actionLoading");

		this.actionChangeButton = document.getElementById("actionChangeButton");
		this.actionCollectButton = document.getElementById("actionCollectButton"); // collect modal - name changed isntead of remove if logged in
		this.actionDeleteButton = document.getElementById("actionDeleteButton"); // drop modal
		this.actionDropButton = document.getElementById("actionDropButton");
		this.actionEditButton = document.getElementById("actionEditButton"); // drop modal
		this.actionHealButton = document.getElementById("actionHealButton");
		this.actionNewButton = document.getElementById("actionNewButton");
		this.actionRunButton = document.getElementById("actionRunButton");
		this.actionPlaceButton = document.getElementById("actionPlaceButton");		
		this.actionUpdateButton = document.getElementById("actionUpdateButton");
		this.actionTestButton = document.getElementById("actionTestButton");

		this.actionUpdateForm = document.getElementById("actionUpdateForm");
		this.actionNameInput = document.getElementById("actionNameInput");

		this.actionScriptForm = document.getElementById("actionScriptForm");
		this.actionScriptname = document.getElementById('actionScriptname');
		this.actionScripttext = document.getElementById('actionScripttext');	

		this.actionStack = document.getElementById('actionStack');
		this.actionStackScripts = document.getElementById('actionStackScripts');
		this.actionStackScriptClone = document.getElementById('actionStackScriptClone').cloneNode();
		this.actionStackScriptCloneFilename = document.getElementById('actionStackScriptCloneFilename').cloneNode();
		this.actionStackScriptCloneImg = document.getElementById('actionStackScriptCloneImg').cloneNode();
		this.actionStackScriptCloneAuthor = document.getElementById('actionStackScriptCloneAuthor').cloneNode();

		this.actionOutputTitle = document.getElementById("actionOutputTitle");
		this.actionOutputTitleImg = document.getElementById("actionOutputTitleImg");
		this.actionOutputLoading = document.getElementById("actionOutputLoading");
		this.actionOutputContent = document.getElementById("actionOutputContent");		
		
		this.actionUpdateForm.addEventListener('submit', function (evnt){
			evnt.preventDefault();
			updateSend();
		});

		this.actionChangeButton.addEventListener("click", function(){
			actionModal.showUpdate(datastore['userData']);
		});

		this.actionEditButton.addEventListener('click', function(){editSend();})
		this.actionDeleteButton.addEventListener('click', function(){deleteSend();})
		this.actionDropButton.addEventListener('click', function(){dropSend();})
		this.actionCollectButton.addEventListener('click', function(){collectSend();})
		this.actionPlaceButton.addEventListener('click', function(){placeSend();})
		this.actionTestButton.addEventListener('click', function(){testSend();})
		this.actionRunButton.addEventListener('click', function(){runSend();})

		this.actionHealButton.addEventListener('click', function(){healSend();})
		this.actionNewButton.addEventListener('click', function(){newSend();})

		this.components = [this.actionTitle, this.actionContent,
						   this.actionButtons, this.actionForm, this.actionStackScripts];
		this.outputComponents = [this.actionOutputTitle,
								 this.actionOutputContent];

		this.clear();
		this.clearOutput();
	}


	//////////////  api /////////////////////////

	appenduserHeader(data, ele, eleImg, useOutput) {
		let userData = data["user"];
		let username = userData["username"];
		if (useOutput)
			username = "Output";
		let form = userData["form"];
		let imgdir = "images/characters/"
		let img = imgdir+form+"/"+form+"WalkDown.gif";
		messageLog(ele, "<h2 style='float:right'; class='text-capitalize'>"
				  +username+"</h2>");
		eleImg.setAttribute('src',img);
		ele.appendChild(eleImg);
	}

	appendUserTitle(data) {
		this.appenduserHeader(data, this.actionTitle, this.actionTitleImg);
	}

	appendOutputUserTitle(data) {
		this.appenduserHeader(data, this.actionOutputTitle, this.actionOutputTitleImg, true);
	}

	clear() {
		for (let component of this.components) 
			while (component.lastChild) 
				component.removeChild(component.lastChild);
		this.loadingOff();
	}

	clearOutput() {
		for (let component of this.outputComponents) 
			while (component.lastChild) 
				component.removeChild(component.lastChild);
		this.loadingOutputOff();
		actionModal.actionOutputContent.value = ''
	}

	loadScriptViewer() {
		this.actionStack.style.display = "none"

		let scriptCount = 0;
		for (let obj of window.selectedObjects)
			if (checkObject(obj) === "script")
				scriptCount++;

		if (scriptCount > 1){
			this.actionStack.style.display = "block"
			for (let item of window.selectedObjects){
				if (checkObject(item) === "script"){
					let data = item.objData;
					let id = data['id']
					let collectedList = data['collected_list']
					let title = data['filename']
					let author = data['user']['username'];	
					let img = "images/icons/bash.png"
					let scriptDiv = this.actionStackScriptClone.cloneNode();
					if (checkScript(id))
						scriptDiv.style.opacity = "0.5"
					scriptDiv.setAttribute("data-script", JSON.stringify(data));
					scriptDiv.addEventListener('click', actionModal.updateCollect);

					let titleDiv = this.actionStackScriptCloneFilename.cloneNode();
					let imgDiv = this.actionStackScriptCloneImg.cloneNode();
					let authorDiv = this.actionStackScriptCloneAuthor.cloneNode();

					imgDiv.setAttribute('src', img);
					titleDiv.appendChild(imgDiv);
					titleDiv.innerHTML = titleDiv.innerHTML + " "+ title;
					scriptDiv.appendChild(titleDiv);
					authorDiv.innerHTML = author;
					scriptDiv.appendChild(authorDiv);

					this.actionStackScripts.appendChild(scriptDiv);
				}
				else if (checkObject(item) === 'character'){

				}
				/// add scripts to actionstackscripts
			}
		}
	}

	loadingOn(){loadingLog();}
	loadingOff(){loadingClear();}
	loadingOutputOn(){loadingOutputLog();}
	loadingOutputOff(){loadingOutputClear();}

	printUser(data){
		if (checkError(data))
			printError(this.actionLoading, data);
		else 
			printUser(this.actionContent, data);
	}

	messageLog(msg) {
		messageLog(this.actionLoading, msg);
	}



	showUpdate(data) {
		this.clear();
		this.actionNameInput.value = datastore.username;
		this.updateUser(data);
		this.showUser(data);
		this.actionForm.appendChild(this.actionUpdateForm);
	}

	showUser(data) {
		this.clear();
		this.appendUserTitle(data);
		this.printUser(data);
	}

	showHeal(data) {
		this.showUser(datastore['userData']);
		this.updateUser();
		//this.actionButtons.appendChild(this.actionChangeButton);
		this.actionButtons.appendChild(this.actionHealButton);
		this.actionButtons.appendChild(this.actionNewButton);
	}

	showDrop(data) {
		this.clear();
		this.appendUserTitle(data);
		this.updateUser(data);

		actionModal.actionScriptname.value = '';
		actionModal.actionScripttext.value = '';
		// actionModal.actionScriptname.removeAttribute("disabled");
		// actionModal.actionScripttext.removeAttribute("disabled");
		this.actionForm.appendChild(this.actionScriptForm);

		if (notGhost())
			this.actionButtons.appendChild(this.actionDropButton);
		this.actionButtons.appendChild(this.actionTestButton);
		this.actionButtons.appendChild(this.actionRunButton);
	}

	updateCollectModal(data) {
		actionModal.actionForm.appendChild(actionModal.actionScriptForm);
		let userData = data['user']
		let scriptid = data['id'];
		let material = data['material'];
		// let location = data['location'];
		let scriptname = data['filename'];
		let scripttext = data['filetext'];
		// let scripttype = data['filetype'];
		let collectedList = data['collected_list'];
		window.selectedID = scriptid;
		let hasCollected = searchList(collectedList);
		let img = "<img src='images/icons/material.gif' width=10 height=15>"
		loadingReturn(material +" " + img + "");

		actionModal.appendUserTitle(data);
		actionModal.actionScriptname.value = scriptname;
		actionModal.actionScripttext.value = scripttext;

		if (connected){
			let collectorX = window.datastore['userData']['user']['username'] == 'CollectorX';
			actionModal.actionButtons.appendChild(actionModal.actionPlaceButton);
			if (!(window.user.ID === data['user']['id']) && notGhost()){
				if (hasCollected)
					actionModal.actionCollectButton.innerHTML = "Recollect";
				else
					actionModal.actionCollectButton.innerHTML = "Collect";
				actionModal.actionButtons.appendChild(actionModal.actionCollectButton);
			}
			else {
				if (actionModal.actionButtons.contains(actionModal.actionCollectButton))
					actionModal.actionButtons.removeChild(actionModal.actionCollectButton);

				if (window.user.ID === data['user']['id']) {
					//console.log(window.datastore['userData']['user']['username'])
					actionModal.actionButtons.appendChild(actionModal.actionEditButton);
					actionModal.actionButtons.appendChild(actionModal.actionDeleteButton);
				}
			}
			if (collectorX) {
				actionModal.actionButtons.appendChild(actionModal.actionCollectButton);
				actionModal.actionButtons.appendChild(actionModal.actionEditButton);
				actionModal.actionButtons.appendChild(actionModal.actionDeleteButton);
			}
		}
		actionModal.actionButtons.appendChild(actionModal.actionTestButton);
		if (connected)
			actionModal.actionButtons.appendChild(actionModal.actionRunButton);
	}

	updateCollect() {
		let data = JSON.parse(this.getAttribute("data-script"));
		actionModal.updateCollectModal(data);
		
	}
	
	showCollect() { // listener added to scripts in Map.js
		actionModal.clear();
		actionModal.actionForm.appendChild(actionModal.actionScriptForm);
		let data = JSON.parse(this.getAttribute("data-script"));
		actionModal.updateCollectModal(data);
	}

	showError() {
		actionModal.clear();
		let output = datastore['error']['msg'];
		// actionModal.actionContent.innerHTML = '<span style="color: darkred;">' + output+ '</span>';
		messageLog(actionModal.actionLoading, '<span style="color: darkred;">' + output+ '</span>')
		messageLog(actionModal.actionOutputLoading, '<span style="color: darkred;">' + output+ '</span>')
	}

	showOutput(output) {
		actionModal.clearOutput();
		actionModal.appendOutputUserTitle(datastore['userData']);
		actionModal.actionOutputContent.value = output;
	}

	updateCollectButtons() {
		actionModal.actionCollectButton.innerHTML = "Recollect";
		// while (actionModal.actionButtons.lastChild) 
		// 	actionModal.actionButtons.removeChild(actionModal.actionButtons.lastChild);
		// actionModal.actionButtons.appendChild(actionModal.actionTestButton);
		// actionModal.actionButtons.appendChild(actionModal.actionRunButton);
	}

	updateUser() {
		let data = datastore['userData'];
		user.changeID(data["user"]["id"]);

		// volitile
		user.character = data["user"]["character"]
		let form = data["user"]["form"];
		if (form === 'ghost')
			user.changeForm(true)
		else
			user.changeForm(false)
		//user.form = data["user"]["form"];
		user.username = data["user"]["username"];
		user.location = data["user"]["location"];
		user.total_collected = data["user"]["total_collected"];
		user.total_dropped = data["user"]["total_dropped"];
		user.update(user.row, user.col)
	}

}